# Sequence learning recodes cortical representations instead of strengthening initial ones

Replicates the fMRI experiment (bioRxiv 496893; http://biorxiv.org/cgi/content/short/496893). Takes the raw MRI data in [BIDS format](https://bids-specification.readthedocs.io/en/stable/) and performs the analyses required to replicate the results, including all figures and tables presented in the manuscript. The MRI data in BIDS format is available from https://www.mrc-cbu.cam.ac.uk/publications/opendata/.

## Pre-requisites

The analysis pipeline uses containers to make the sofware environment easily replicable. 
Two separate containers are required:
* Fmriprep preprocessing image
* Data analysis image, containing Nipype and Jupyter Lab

The containers can be run with either [docker](https://www.docker.com/) or [singularity](https://singularity.lbl.gov/) as described below. 

You also need [FreeSurfer](https://surfer.nmr.mgh.harvard.edu/) (>=6.0) installed on your system. [FreeSurfer can also be run containerised](https://github.com/freesurfer/freesurfer/blob/dev/Dockerfile), but to make it work you need to modify the nipype dependencies or call the analysis container from the FreeSurfer container. 

## Pre-processing

Create a fmriprep preprocessing image on your system:

    docker pull docker://poldracklab/fmriprep:1.4.1
or

    singularity pull docker://poldracklab/fmriprep:1.4.1

All anatomical and functional pre-processing was carried out with fmriprep ([v1.4.1](https://fmriprep.readthedocs.io/en/stable/changes.html#october-31-2018)). Every participant was processed using the same preprocessing pipeline inititated with:

```bash
fmriprep --participant_label $participant_label \
    --fs-license-file $fs_license_file \
    --notrack --stop-on-first-crash --write-graph --verbose \
    --no-submm-recon --force-bbr --bold2t1w-dof 12 \
    $bids_folder $out_folder participant -w $temp_folder
```
See https://fmriprep.readthedocs.io/en/stable/usage.html for detailed description of input arguments.

Run preprocessing with the singularity container:

    singularity run -B $fprep_simgularity_image --participant_label $participant_label \
        --fs-license-file $fs_license_file \
        --notrack --stop-on-first-crash --write-graph --verbose \
        --no-submm-recon --force-bbr --bold2t1w-dof 12 \
        $bids_folder $out_folder participant -w $temp_folder

## Statistical analysis workflows

First create the [analysis image](https://hub.docker.com/repository/docker/kristjankalm/fmri_seq):

    docker pull docker://kristjankalm/fmri_seq:cbu
or

    singularity pull docker://kristjankalm/fmri_seq:cbu

And then run the workflows outlined below as:

    docker run -v /$mybidsdata:$mybidsdata $docker_image python $path-to-script

where '$mybidsdata' points to the BIDS dataset outside the container. 

For singularity:

    singularity run -B /$mybidsdata:$mybidsdata $singularity_image python $path-to-script

where '$singularity_image' is the full path to the .simg file that was created by issuing 'singularity pull' as described above. 

For example, on my system I run:

    singularity run \
        -B /home/kk02:/home/kristjan \
        -B /imaging \
        /imaging/local/software/singularity_images/fmri_seq-cbu.simg \
        python /home/kristjan/Code/python/ltm/nipype_flows/label.py

Here '/home/kristjan/Code/python/ltm' is the location of this repository. You also must [set the location for the BIDS files in Paths.py](https://gitlab.com/kristjankalm/fmri_seq_ltm/-/blob/master/flow/Paths.py#L9).

Depending on your system and priviledges you might need to specify FreeSurfer environment variables separately for the container. The two necessary environment variables are FREESURFER_HOME and SUBJECTS_DIR. For example, on my system I specify them as:

    export FREESURFER_HOME=/imaging/local/software/freesurfer/6.0.0/x86_64/
    export SUBJECTS_DIR=/imaging/kk02/data/ltm/fprep/freesurfer
    
In most cases containers can access host environment variables, but if that is not possible on your system you can send them directly to the containe; for example for singularity:

    SINGULARITYENV_APPEND_PATH=${FREESURFER_HOME}/bin singularity run \
        -B /home/kk02:/home/kristjan \
        -B /imaging \
        /imaging/local/software/singularity_images/fmri_seq-cbu.simg \
        python /home/kristjan/Code/python/ltm/nipype_flows/label.py

The example above makes sure that FreeSurfer binaries are found when running the script on the container. 

### 1. Extract anatomical regions

https://gitlab.com/kristjankalm/fmri_seq_ltm/blob/master/nipype_flows/label.py

![Alt text](nipype_flows/flow_graphs/label_aparc_2009.png)

### 2. Estimate GLMs

https://gitlab.com/kristjankalm/fmri_seq_ltm/blob/master/nipype_flows/glm.py

![Alt text](nipype_flows/flow_graphs/glm.png)

### 3. Representational similarity analyses (RSA)

https://gitlab.com/kristjankalm/fmri_seq_ltm/blob/master/nipype_flows/rsa.py

![Alt text](nipype_flows/flow_graphs/rsa.png)

## Group level analyses

### 1. RSA, group level

https://gitlab.com/kristjankalm/fmri_seq_ltm/blob/master/notebooks_fig/fig_rfx.ipynb

### 2. Behavioural measures

https://gitlab.com/kristjankalm/fmri_seq_ltm/blob/master/notebooks/stats_behav.ipynb

## Figures

https://gitlab.com/kristjankalm/fmri_seq_ltm/tree/master/notebooks_fig


{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Predicted representational similarity between two encoding models\n",
    "\n",
    "What is the expected similarity between representations of sequences, where one set is encoded with item-position model and the other with item-item model?\n",
    "\n",
    "These two sequence representation models predict different between-sequence similarity: the predictions themselves are negatively correlated as shown on the figure below (IP: Item-Position, II: Item-Item):\n",
    "\n",
    "<img src='fig/ip_vs_ii.png'> \n",
    "\n",
    "This negative similarity means that when IP model predicts high similarity between two sequences then II model predicts low similarity between the two. However, for both individual similarity measurements, *both of the sequences* use the same encoding model. However, our question here is different: what happens when one sequence is encoded with IP and the other with II? This requires estimating similarity between two sequences which use different encoding schemes.\n",
    "\n",
    "The negative correlation -0.25 is the similarity between the II and IP predictions, and not similarity between two sequences with different encodings (to see how the similarity between predictions is calculated see this notebook [notebooks_fig/fig_models.ipynb]). In other words, we can only predict similarity within sequences all encoded as II or IP, and compare these two matrices to each other, but we have no tools to derive a similarity of II-encoded sequence to a IP-encoded sequence. \n",
    "\n",
    "Next we simulate a neural population where one set of sequences is represented with IP and the other with II model and explore what is the expected similarity between theses two set of representations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sequence codes\n",
    "\n",
    "Sequence 'CADB' in a matrix form using item-position (IP) associations:\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/ip-0.png'>\n",
    "\n",
    "This matrix can be reshaped into a vector where (as in the matrix) each cell maps a single item-position association:\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/units_ip_1.png'>\n",
    "\n",
    "This vector/matrix can be thought of as a set of units, each responsive to a particular item-position association. This is the simplest form of representation where each cell responds in a binary manner to a single association (no noise, binary responses, one unit per mapping). \n",
    "\n",
    "Responses to all presented sequences during the experiment can be represented as a matrix, each row corresponding to an individual sequence, each column to a unit mapping a single item-position correspondence, and the whole matrix characterising the population's response to a set of stimuli:\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/units_ip_s0.png'>\n",
    "\n",
    "An item-item associations model (II) based on bigrams can be represented in the same form, except now units are sensitive to particular item-item mappings:\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/units_ii_s0.png'>\n",
    "\n",
    "To make the simulation a more realistic model of an ROI in the brain, we can add noise in various ways: change binary responses to preference distributions, add units that don't respond to sequences at all, add measurement noise etc.\n",
    "\n",
    "Next, lets make half of the sequences have IP codes and the other half II codes and measure what is the similarity between these subsets. In the matrix below, the first 7 sequences are represented with IP and the remaining 7 with II bigram codes. Since II codes are similar to 2-items chunks, lets call the first 7 sequences 'novel' and last 7 'learned: though completely artificial, this simulates a case where in a single ROI we have a set of sequences encoded with positional associationa and another set of 'learned' sequences, where codes have changed, i.e. a different encoding model is used.\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/units_btw.png'>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extected similarity between two sets of representations\n",
    "We can now calculate the similarity between 'novel' and 'learned' sequences (the box shows the cells which represent similarity between first 7 and last 7 codes):\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/ip_vs_ii.png'>\n",
    "\n",
    "The average correlation value over these cells is -0.06; the correlation to the IP prediction (both novel and learned encoded positionally) is -0.16; the correlation to the II prediction (all sequences encoded with bigrams) is -0.05. None of these values are equal to the negative correlation between IP and II predictions of -0.25 on the figure above (first paragraph), because it measures a different thing. \n",
    "\n",
    "Re-running this simulation $10^3$ times gives an average correlation between these two sets of codes as 0.00002 (std=0.003). This shows that a particular choice of encoding model parameters (which item or position is encoded with what value) does not matter. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Same units encode novel and learned sequences?\n",
    "\n",
    "It is clear that if there is no overlap between units that encode novel and learned sequences then the similarity between novel and learned would be zero or just noise. This is independent of the encoding scheme: both novel and learned ones could be encoded with the same model but if there are two separate subsets then the avergage similarity between those would be zero. \n",
    "\n",
    "*However, if we observe significantly different that zero values for any ROI, this indicates that there must be an overlap*, and the nature of the overlap could potentially be inferred by the size of the effect. \n",
    "\n",
    "In sum, this means that we can more or less ignore evaluting this assumption: if we get a significantly positive or negative correlation across subjects for an ROI, this means that there must be an overlap. The exact amount of the overlap doesn't seem to matter here, since it's dependent on measurement noise etc (how units/voxels are defined, movement artefacts between sequences, diffeent ROI definitrions for different subjects etc.)\n",
    "\n",
    "### IP and II codes share a dimension: item representations.\n",
    "\n",
    "In the simulation above, IP and II bigram codes share item representations. This means that in the positional model there are four units that map A to four positions, and in the bigram model *the same four units* map A to the second item in the bigram. In other words, this means that two models share one dimension: positional models have two dimensions (items, positions), this bigram model here has also two (items_1,items_2) and here 'items=items_1'. \n",
    "\n",
    "This creates consistency across encoding schemes: same units respond consistently to the values of the first dimension. Is this a reasonable assumption? Yes and no. Yes, since if a set of units has to encode both novel and learned, this consistent scheme requires less bits to encode than not sharing the first dimension. Encoding them separately would require more units. \n",
    "\n",
    "However, as we outline in the 'Associative sequence learning is theoretically suboptimal for long-term storage' section of the manuscript (in Discussion), a recoding mechanism (e.g chunking) which shares item codes with initial associations, will run into the same problems of learning multiple overlapping sequences as associative codes.\n",
    "\n",
    "In sum, recoded representations could share dimensions with initial representations or not: there are information theoretic advantages for both. However, in both cases the expected similarity between the two sets of codes is still zero.   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Within vs. between sequence similarity\n",
    "\n",
    "Although we cannot meaningfully estimate similarity between representations of sequences, where one set is encoded with item-position model and the other with item-item model, we can meaningfully estimate similarities *within each subset*. \n",
    "\n",
    "If the units encode some sequences with IP and some with II bi-grams then within-subset similarlity is still predictable: 'novel' sequences should be similar to each other as predicted by IP and 'learned' to each other as predicted by II-bigram. These are the RDM areas on top and right of the box below (which marcates between-type or novel-learned similarity).\n",
    "\n",
    "<img src='https://gitlab.com/kristjankalm/fmri_seq_ltm/-/raw/master/notebooks_rev/fig/ip_vs_ii.png'>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

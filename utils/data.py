def common_columns(D, F):
    import numpy as np
    from scipy.spatial.distance import cdist
    dM = cdist(F.T, D.T, 'correlation')
    idx = np.where(dM < 10**-6)
    print(dM.shape, F.shape)
    idx_f = idx[0]
    return idx_f
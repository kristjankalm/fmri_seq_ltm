import numpy as np
# import pandas as pd
from scipy.spatial.distance import cdist, squareform
from scipy.stats import spearmanr

import warnings
warnings.filterwarnings("ignore")


def upper_tri_indexing(A):
    # returns the upper triangle
    m = A.shape[0]
    r, c = np.triu_indices(m, 1)
    return A[r, c]


def progress(i, _length):
        if i % 250 == 0:
            print(str(round(i/_length*100, 1)) + '%', end='\r')


class SearchLight():
    def __init__(self, mask, radius=1, thr=.7):
        """
        Parameters:
            mask:    3d spatial mask (of usable voxels set to 1)
            radius:  radius around each center (in voxels)
            thr :    proportion of usable voxels necessary
                     thr = 1 means we don't accept centers with voxels outside
                     the brain
        """
        self.mask = mask
        self.radius = radius
        self.thr = thr
        self.centers, self.ind = self._findCenters()
        self.dist = None # voxel pattern distances (RDM)
        self.NaNs = []
        self.R = None # sl scores: 3D image
        self.z = None # sl scores: 1d array

        assert type(mask) is np.ndarray, "Mask has to be 3D numpy array!"

    def _findCenters(self):
        """
        Find all indices from centers with usable voxels over threshold.
        """
        # make centers a list of 3-tuple coords if not given
        print('Finding centres...')

        centers = zip(*np.nonzero(self.mask))

        good_center = []
        good_ind = []
        for center in centers:
            ind = self.searchlightInd(center)
            if self.mask[ind].mean() >= self.thr:
                good_center.append(center)
                good_ind.append(ind)
        return good_center, good_ind


    def searchlightInd(self, center):
        """Return indices for searchlight where distance < radius

        Parameters:
            center: point around which to make searchlight sphere
        Sets RDM variable to:
            numpy array of shape (3, N_comparisons) for subsetting data
        """
        center = np.array(center)
        shape = self.mask.shape
        cx, cy, cz = np.array(center)
        x = np.arange(shape[0])
        y = np.arange(shape[1])
        z = np.arange(shape[2])

        # First mask the obvious points
        # - may actually slow down your calculation depending.
        x = x[abs(x-cx) < self.radius]
        y = y[abs(y-cy) < self.radius]
        z = z[abs(z-cz) < self.radius]

        # Generate grid of points
        X, Y, Z = np.meshgrid(x, y, z)
        data = np.vstack((X.ravel(), Y.ravel(), Z.ravel())).T
        distance = cdist(data, center.reshape(1, -1), 'euclidean').ravel()

        return data[distance < self.radius].T.tolist()


    def fit_rdm(self, data, metric='correlation'):
        """
        Fit Searchlight for RDM
        Parameters:
            data:       4D numpy array - (x, y, z, condition vols)
            metric :    str or callable, optional
                        The distance metric to use.  If a string, the distance function can be
                        'braycurtis', 'canberra', 'chebyshev', 'cityblock', 'correlation',
                        'cosine', 'dice', 'euclidean', 'hamming', 'jaccard', 'kulsinski',
                        'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto', 'russellrao',
                        'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean',
                        'wminkowski', 'yule'.
        """
        # brain = np.zeros((x, y, z, rdm_size, rdm_size))
        distances = []
        cen_length = len(self.centers)
        print('Computing data RDMs: ' + str(cen_length))

        for i, c in enumerate(self.centers):
            # print progress
            progress(i, cen_length)
            # Get indices from center
            # ind = np.array(self.searchlightInd(c))
            ind = np.array(self.ind[i])
            X = np.array([data[f[0], f[1], f[2], :] for f in ind.T]).T
            dist = upper_tri_indexing(cdist(X, X, metric))
            distances.append(dist)

        self.dist = distances


    def rsa(self, Filter, hRDM, **kwargs):

        if 'labels' in kwargs:
            Labels = kwargs.get('labels')
            analysis_btw = True
        else:
            analysis_btw = False

        cen_length = len(self.centers)
        print('Computing rho(stimulus_RDM, data_RDM): ' + str(cen_length))

        z = np.full_like(np.arange(len(self.centers)), np.nan, dtype=np.double)  # variable for score

        # assert data and hRDM
        sRDM = hRDM[Filter][:, Filter]
        upper_sRDM = upper_tri_indexing(sRDM)

        if analysis_btw:
            n = sRDM.shape[0]
            J = np.zeros((n, n), dtype=bool)
            IsLearned = np.array(Labels, dtype=bool)
            J[np.ix_(~IsLearned, IsLearned)] = True

        for i, c in enumerate(self.centers):
            # print progress
            progress(i, cen_length)
            
            # check if data has at least N entries
            if not np.isnan(np.array(self.dist[i])).any():  # require full data rdm
                rdm = squareform(self.dist[i])
                dRDM = rdm[Filter][:, Filter]
                if analysis_btw:
                    sr = spearmanr(dRDM[J], sRDM[J])
                else:
                    sr = spearmanr(upper_sRDM, upper_tri_indexing(dRDM))
                z[i] = sr.correlation

        self.R = np.zeros(self.mask.shape)
        c = np.asarray(self.centers)
        self.R[c[:, 0], c[:, 1], c[:, 2]] = z
        self.z = z


    def svm(self, Filter, Label, data):

        from sklearn.model_selection import cross_val_score, KFold
        from sklearn.svm import SVC
        from nilearn import signal

        splits = round(len(Label) / 2)
        print('Cross-val splits: ' + str(splits))

        CV = KFold(n_splits=splits)
        svc = SVC(kernel='linear', C=1)
        # svc = SVC(kernel='rbf', gamma='scale')

        cen_length = len(self.centers)
        print('SVM: ' + str(cen_length))

        z = np.full_like(np.arange(len(self.centers)), np.nan, dtype=np.double)  # variable for score

        for i, c in enumerate(self.centers):
            # print progress
            progress(i, cen_length)

            # Get indices from center
            # ind = np.array(self.searchlightInd(c))
            ind = np.array(self.ind[i])
            X = np.array([data[f[0], f[1], f[2], :] for f in ind.T]).T
            F = X[Filter, :]
            F = signal.clean(F)
            cv_score = cross_val_score(svc, F, Label, cv=CV)
            z[i] = cv_score.mean()

        self.R = np.zeros(self.mask.shape)
        c = np.asarray(self.centers)
        self.R[c[:, 0], c[:, 1], c[:, 2]] = z
        self.z = z

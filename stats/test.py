import os, sys
import numpy as np
import pandas as pd
from scipy import stats, io

import warnings
warnings.filterwarnings('ignore')

sys.path.append(os.path.join(os.path.dirname(os.getcwd())))  # add parent folder to path
from flow import Paths

def perm_sample(g, test_value=0, alpha=10**-3):
    # g is a matrix of permutations x subjects
    assert alpha >= 0, 'Sign threshold is not < 0'
    num_samples = int(10 / alpha)
    # print('10^' + str(np.log10(num_samples)) + ' samples')
    group_size = g.shape[1]
    P = np.full((group_size, num_samples), np.nan)
    for s in range(group_size):
        P[s,:] = np.random.choice(g[:,s], num_samples)
    group_means = np.nanmean(P, axis=0)
    group_means = np.append(group_means, test_value)
    p = 1 - np.sum(group_means < test_value) / num_samples
    return p


def repval(value):
    if value < 10**-6:
        s = '***'
    elif value < 10**-5:
        s = '**'
    elif value < 10**-4:
        s = '*'
    else:
        s = ''
    return s

def binary_regression(X, Y):
    from sklearn.model_selection import cross_val_score, KFold
    from sklearn.svm import SVR as r2_regression
    from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import StandardScaler
    from nilearn import signal
    import numpy as np

    score = np.nan
    if type(Y) == np.ndarray:  
        Y = signal.clean(Y)
        CV = KFold(n_splits = 2)
        regressor = r2_regression(kernel='linear')
#         regressor = make_pipeline(StandardScaler(), r2_regression(kernel='linear'))
#         regressor = make_pipeline(r2_regression(C=1.0, kernel='linear'))
        cv_score = cross_val_score(regressor, Y, X, cv = CV)
        score = np.nanmean(cv_score) # convert r^2 to r
    return score

def binary_r(X, Y):
    from sklearn.model_selection import cross_val_score, KFold
    from sklearn.svm import LinearSVC as linear_kernel
    from nilearn import signal
    import numpy as np
    from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import StandardScaler

    chance_level = 1 / np.unique(X).shape[0]
    score = np.nan
    if type(Y) == np.ndarray:  
        Y = signal.clean(Y)
        CV = KFold(n_splits = int(len(X)/1))
        regressor = make_pipeline(StandardScaler(), linear_kernel())
        regressor = make_pipeline(linear_kernel())
        cv_score = cross_val_score(regressor, Y, X, cv = CV)
        score = cv_score.mean() - chance_level
    return score

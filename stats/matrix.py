import numpy as np


def squareform(upper_tri_data):
    # converts vector of upper triangle values to a diagonally identical matrix
    # with ZEROS on the diagonal: this applies to DISTANCES and not similarities
    len_u = len(upper_tri_data)
    n = int(np.ceil((2 * len_u) ** (1 / 2)))  # ceil(sqrt(2*n))
    M = np.zeros((n, n))
    i_lower = np.tril_indices(n, -1)
    i_upper = np.triu_indices(n, 1)
    M[i_upper] = upper_tri_data
    M[i_lower] = M.T[i_lower]
    return M


def upper_tri_vector(A):
    # returns the upper triangle data in 1d
    m = A.shape[0]
    r, c = np.triu_indices(m, 1)
    return A[r, c]


def scale_zero_one(I, upper_triangle=False):

    if upper_triangle:
        I = upper_tri_vector(I)

    I = np.array(I)
    I = (I - I.min()) / (I.max() - I.min())  # scale IM btw 0,1

    if upper_triangle:
        I = squareform(I)

    return I
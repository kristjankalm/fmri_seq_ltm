import os
import sys
import warnings
import numpy as np
import pandas as pd
from scipy import stats, io
from scipy.spatial.distance import cdist

sys.path.append(os.path.join(os.path.dirname(os.getcwd())))
from flow import Paths
from seq import ngram, dist, utils
from flow import Paths, design, noise
from stats import test


def spearman_to_z(rho):

    assert rho.shape[0] > 1, 'Must be a column'

    n = rho.shape[0]
    f_rho = np.arctanh(rho)
    z = np.sqrt( (n-3  / 1.06)) * f_rho
    return z


def create_stim_rdms():
    Folder, _, _, Subjects = Paths.ExpData()
    file_events = os.path.join(Folder['SourceData'], 'trial_phase_events.tsv')
    E = pd.read_csv(file_events, sep='\t') # get sequences
    Seq_phase = E['Seq'][E['Phase'] < 4] # remove rest phase sequences
    # convert integers to sequence arrays
    Seq_phase = [[int(i) for i in str(sp)] for sp in Seq_phase]
    Seq_phase = np.array(Seq_phase, dtype='uint8')
    
    #hamming
    H = cdist(Seq_phase, Seq_phase, 'hamming')
    # 2-gram
    N2 = 1 - ngram.rdm(Seq_phase, 2)
    # 3-gram
    N3 = 1 - ngram.rdm(Seq_phase, 3)
    # item mixture
    Beta = np.array([0.6, 0.3, 0.1, 0]) # from gradient parameter
    I = dist.item_mixture(Seq_phase, Beta)
    io.savemat(os.path.join(Folder['csv'], 'rdm_stim_phase.mat'), 
               mdict={'H': H, 'N2': N2, 'N3': N3, 'IM': I})  # save MAT
    

def get_scores(Models, Phases, Analyses):
    
    Folder, _, _, Subjects = Paths.ExpData()
    RoiLabels = pd.read_csv(os.path.join(Folder['SourceData'], 'vois_aparc_2009_labels.tsv'), index_col=0)

    R = []
    Rs = []

    subs = [s for s in range(len(Subjects))]

    for p, phase_label in enumerate(Phases):

        for a in Analyses:

            islearned = a[0]
            labelname = a[1]
            phase = p + 1
            islearned_str = ''.join([str(l) for l in islearned])

            FilterDict = {'Phase': [phase], 'IsLearned': islearned}
            Filter, Label = design.get_design(FilterDict, LabelName=labelname)

            for model in Models:

                testModels = ['Score', 'IM'] # test for model + item-mixture (IM) null-model

                # data RDMs
                Vois = list(RoiLabels.index.values)
                R_rho, R_rnk, R_f = noise.rdms(Subjects, Vois, Filter, phase=str(phase), model=model)

                # noise
                RDM_LB_voi, RDM_UB_voi, _, RDM_LB_p = noise.ceiling(R_rnk, Subjects, Vois)

                fModels = [design.get_RDM(Model=m)[Filter][:, Filter] for m in [model, 'IM']]
                RDM_fit, RDM_ebar, tval, pval, rho = noise.fit(R_rho, R_f, fModels, Subjects, Vois, Label)

                # results to dataframe
                d = pd.DataFrame({'Hemi' : RoiLabels['Hemi'], 'Lobe' : RoiLabels['Lobe'], 'Name': list(RoiLabels['Name']), 
                                  'Voi': RoiLabels.index, 'IsLearned': islearned_str, 'Phase': phase_label, 'Model': model,
                                  'UB': RDM_UB_voi, 'LB': RDM_LB_voi, 'LB_p': RDM_LB_p})
                e = pd.DataFrame(data=RDM_ebar.T, columns = [m + '_e' for m in testModels])
                g = pd.DataFrame(data=tval.T, columns = [m + '_t' for m in testModels])
                h = pd.DataFrame(data=pval.T, columns = [m + '_p' for m in testModels])
                f = pd.DataFrame(data=RDM_fit.T, columns = testModels)
                y = pd.concat([d, f, e, g, h], axis=1)

                # subject-level data
                ys_ = []
                for sb in subs:
                    ys = pd.DataFrame({'Hemi' : RoiLabels['Hemi'], 'Lobe' : RoiLabels['Lobe'], 'Name': list(RoiLabels['Name']), 
                                       'Voi': RoiLabels.index, 'IsLearned': islearned_str, 'Phase': phase_label, 'Model': model,
                                       'UB': RDM_UB_voi, 'LB': RDM_LB_voi, 'LB_p': RDM_LB_p, 'R': rho[sb,0,:], 'Subject': sb})
                    ys_.append(ys)
                    yS = pd.concat(ys_, axis=0)

                R.append(pd.DataFrame(data=y))
                Rs.append(pd.DataFrame(data=yS))

    B = pd.concat(Rs, ignore_index=True)
    Y = pd.concat(R,  ignore_index=True)

    file_stats = os.path.join(Folder['csv'], 'stats.csv')
    file_scores = os.path.join(Folder['csv'], 'scores.csv')
    
    Y.to_csv(file_stats)
    B.to_csv(file_scores)
    
    return Y, B
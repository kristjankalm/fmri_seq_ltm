import sys, os
import pandas as pd
import numpy as np
from brainiak.utils import fmrisim
from scipy.spatial.distance import cdist, squareform
from scipy.stats import spearmanr

sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from stats.matrix import upper_tri_vector
from flow import Paths

home = os.environ.get('HOME') 
sys.path.append(os.path.join(home, 'Code/python'))
from seq import rep



def unit_resp(sigma, TR, fq):

    # Load experimental design
    Folder, _, _, _ = Paths.ExpData()
    file_seq = os.path.join(Folder['Bids'], 'sequences_individual.tsv')  # Import all sequences from CSV file.
    Seq = pd.read_csv(file_seq).values  # load individual sequences
    file_exp = os.path.join(Folder['Bids'], 's1_task.tsv')  # Import all sequences from CSV file.
    T = pd.read_csv(file_exp, index_col=0)  # load individual sequences

    event_idx = T['Seq_Id'].unique()
    event_idx.sort()
    duration = sum(np.array(T['Duration']))
    t = np.zeros((int(np.round(duration * fq)), len(event_idx)))

    for i, s in enumerate(event_idx):
        onsets = np.array(T['Onset'][T['Seq_Id'] == s])
        t[:, i:i + 1] = fmrisim.generate_stimfunction(
            onsets=onsets,
            event_durations=[TR * 4 * 2],
            total_time=duration,
            temporal_resolution=fq
        )

    # Convert to population response matrix.
    y = rep.seq_unit_matrix(Seq, model='ip', sigma=sigma)
    w = t @ y  # apply voxel responses to events

    return w


def noise(Subject='03'):

    from nilearn.image import load_img
        
    File, Folder = Paths.SubjectData(Subject)
    
    
    file_noise_dict = os.path.join(Folder['SubOut'], 'noise_dict.npy')
    file_noise_func = os.path.join(Folder['SubOut'], 'noise_func.npy')

    # load estimated noise
    noise_dict = np.load(file_noise_dict, allow_pickle=True)
    noise_dict = noise_dict.tolist()

    # load noise data
    if os.path.exists(file_noise_func):
        # print('Loading noise data from saved file ...')
        noise_func = np.load(file_noise_func, allow_pickle=True)
    else:        
        print('Loading noise data from image ...')
        file_noise = os.path.join(Folder['SubOut'], 'noise.nii.gz')
        n = load_img(file_noise)
        noise = n.get_data()
        
        # Create the region of activity where signal will appear
        sl = 61;
        coordinates = np.array([[41, 31, sl]])  # Where in the brain is the signal
        feature_size = 5  # How big, in voxels, is the size of the ROI
        signal_volume = fmrisim.generate_signal(
            dimensions=noise.shape[0:3],
            feature_type=['cube'],
            feature_coordinates=coordinates,
            feature_size=[feature_size],
            signal_magnitude=[1]
        )

        # plt.figure()
        # plt.imshow(signal_volume[:, :, sl], cmap=plt.cm.gray)
        # plt.imshow(mask[:, :, sl], cmap=plt.cm.gray, alpha=.5)
        # plt.savefig(os.path.join(Folder['SubOut'], 'noise.png'))
        
        signal_idxs = np.where(signal_volume == 1)
        noise_func = noise[signal_idxs[0], signal_idxs[1], signal_idxs[2], :]
        np.save(file_noise_func, noise_func)
    
    
    return noise_dict, noise_func


def bold(Subject = '03', scaling_magnitude = 0.5, scaling_method = 'CNR_Amp/Noise-SD',
         sigma = 0.5, TR = 2):
    # timing params
    fq = 10

    # unit responses over exp
    w = unit_resp(sigma, TR, fq)

    # Presentation events: onset is at the start of each trial, duration two scans per item i.e. 8 scans per sequence
    y_func = fmrisim.convolve_hrf(
        stimfunction=w,
        tr_duration=TR,
        temporal_resolution=fq,
        scale_function=1
    )

    # ## Noise
    # Estimation of the noise is described in
    noise_dict, noise_func = noise(Subject=Subject)
    noise_func = noise_func[:y_func.shape[1], :y_func.shape[0]]

    # ## Combine noise and expected signal
    # print('Combining noise and expected signal ...')
    y_func_scaled = fmrisim.compute_signal_change(
        y_func,
        noise_func,
        noise_dict,
        magnitude=[scaling_magnitude],
        method=scaling_method
    )
    bold = y_func_scaled.transpose() + noise_func

    return bold


def main(Subject = '03', scaling_magnitude = 0.5, scaling_method = 'CNR_Amp/Noise-SD',
         sigma = 0.5, TR = 1.206):

    # timing params
    hrf_lag = 10

    # Load experimental design
    Folder, _, _, _ = Paths.ExpData()
    file_exp = os.path.join(Folder['Bids'], 's1_task.tsv')  # Import all sequences from CSV file.
    T = pd.read_csv(file_exp, index_col=0)  # load individual sequences

    y_bold = bold(Subject=Subject, scaling_magnitude=scaling_magnitude,
                  scaling_method=scaling_method, sigma=sigma, TR=TR)

    # ## RSA
    # voxel RDM
    trial_scans = np.array((T['Onset'] + hrf_lag) / TR).astype('int')
    y_trials = y_bold[:, trial_scans]
    yy = y_trials.transpose()
    D = cdist(yy, yy, 'correlation')

    # stimulus RDM
    file_s1_seq = os.path.join(Folder['Bids'], 's1_seq.npy')  # Import all sequences from CSV file.
    S = np.load(file_s1_seq)
    H = cdist(S, S, 'hamming')

    # only novel lists
    filter_novel = T['Is_Learned'] == 0
    h_novel = H[filter_novel][:, filter_novel]
    d_novel = D[filter_novel][:, filter_novel]
    sr = spearmanr(upper_tri_vector(h_novel), upper_tri_vector(d_novel))

    # print(sr.correlation)

    return sr.correlation

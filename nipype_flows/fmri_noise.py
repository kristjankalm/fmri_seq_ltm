import os
import sys
from nipype import Workflow, Node
import pandas as pd
from nipype.interfaces.io import DataSink
from nipype.interfaces.utility import Function, IdentityInterface

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import Paths, flow

FlowName = 'fmri_noise'
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
	Subjects = [str(sys.argv[1])]  # get subject code from input

# # 1 -- Subject Id

SubNode = Node(IdentityInterface(fields=['Subject']), name="SubNode")
SubNode.iterables = [('Subject', Subjects)]

# # 2 -- DataSink files -- # #

sink_subs = [('_ni/_Subject_', 'sub-')]

DataSinkNode = Node(
	DataSink(base_directory=Folder['Base'], substitutions=sink_subs),
	name='DataSink')

# # 3 -- estimate noise per subject-- # #

EstimateFmriNoise = Function(
	input_names=['Subject'],
	output_names=['file_noise_dict', 'file_noise_img'],
	function=flow.fmri_noise)
EstimateFmriNoiseNode = Node(EstimateFmriNoise, name='EstimateFmriNoise')


# # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])

Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])
Flow.connect([
	(SubNode, EstimateFmriNoiseNode, [('Subject', 'Subject')]),
	(EstimateFmriNoiseNode, DataSinkNode, [
        ('file_noise_dict', 'nipype.@noise_dict'),
        ('file_noise_img', 'nipype.@noise_img')
    ])
])

Flow.write_graph(graph2use='colored')
n_procs = 1 # len(Subjects)
Flow.run('MultiProc', plugin_args={'n_procs': n_procs})

import subprocess
import os
from nipype import Workflow, Node
from nipype.pipeline.engine import MapNode, JoinNode
from nipype.interfaces.io import DataSink
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.fsl.utils import Merge
from nipype.interfaces.freesurfer import Label2Vol
from nipype.interfaces.ants import ApplyTransforms
from nipype.interfaces.freesurfer.model import Binarize

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import voi, Paths

FlowName = 'label_aparc_2009'  # folder in temp output where temp results go
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
	Subjects = [str(sys.argv[1])]  # get subject code from input

# # -- 1 -- Subject Id
SubNode = Node(IdentityInterface(fields=['Subject']), name="SubNode")
SubNode.iterables = [('Subject', Subjects)]


# # -- 2 -- Subject files and folders

SubData = Function(
	input_names=['Subject'],
	output_names=['subject_folder', 'folder_fs', 'folder_fslabel', 'file_FsMriT1', 'file_ref_MNI', 'files_trans'],
	function=Paths.subdata_label)
SubDataNode = Node(SubData, name='SubData')


# # -- 3 -- Hemi iterate
HemiNode = Node(IdentityInterface(fields=['hemi']), name="HemiNode")
HemiNode.iterables = [('hemi', ['lh', 'rh'])]


# # -- 4 -- Annot 2 Label

Annot2Label = Function(
	input_names=['Subject', 'hemi', 'folder_fs', 'folder_fslabel'],
	output_names=['FilesLabel', 'FileRoiTsv'],
	function=voi.annot2label)
Annot2LabelNode = Node(Annot2Label, name='Annot2Label')


# # -- 5 -- Label 2 Volume
LabelVol = Label2Vol(
	subjects_dir = Folder['Fs'],
	proj=('frac', 0, 1, 0.1),
	identity=True,
	fill_thresh = 0.01,
	)
LabelVolNode = MapNode(LabelVol, name = 'LabelVol', iterfield = ['label_file'])

# # -- 6 -- Volume 2 MNI

TransformNode = MapNode(ApplyTransforms(), name = 'Transform', iterfield = ['input_image'])

# # -- 7 -- Binarise

BinariseNode = MapNode(Binarize(min=0.1, subjects_dir=Folder['Fs']), name = 'Binarize', iterfield = ['in_file'])

# # -- 8 -- Merge vois

MergeVoisNode = Node(Merge(dimension='t', output_type='NIFTI_GZ'), name="MergeVois")

# # -- 9 -- Merge left and right hemis
MergeHemisNode = JoinNode(Merge(dimension='t', output_type='NIFTI_GZ'),
	joinsource="HemiNode",
	joinfield="in_files",
	name="MergeHemis")

# # -- 10 -- DataSink
DataSinkNode = Node(
	DataSink(
		base_directory = Folder['Base'],
		substitutions = [
			('_ni/_Subject_', 'sub-'),
			('voi_data', 'vois_data'),
			('lh.bankssts_vol_trans_thresh_merged_merged', 'vois_aparc'),
			('lh.G_Ins_lg_S_cent_ins_vol_trans_thresh_merged_merged', 'vois_aparc_2009')
		]
		),
	name='DataSink')

# # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])

Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])
Flow.connect([
	(SubNode, SubDataNode, [('Subject', 'Subject')]),
	(HemiNode, Annot2LabelNode, [('hemi', 'hemi')]),
	(SubNode, Annot2LabelNode, [('Subject', 'Subject')]),
	(SubDataNode, Annot2LabelNode, [
		('folder_fs', 'folder_fs'),
		('folder_fslabel', 'folder_fslabel'),
	]),
	(HemiNode, LabelVolNode, [('hemi', 'hemi')]),
	(SubDataNode, LabelVolNode, [
		('file_FsMriT1', 'template_file'),
		('subject_folder', 'subject_id')
		]),
	(Annot2LabelNode, LabelVolNode, [('FilesLabel', 'label_file')]),
	(LabelVolNode, TransformNode, [('vol_label_file', 'input_image')]),
	(SubDataNode, TransformNode, [
		('file_ref_MNI', 'reference_image'),
		('files_trans', 'transforms')
		]),
	(TransformNode, BinariseNode, [('output_image', 'in_file')]),
	(BinariseNode, MergeVoisNode, [('binary_file', 'in_files')]),
	(MergeVoisNode, MergeHemisNode, [('merged_file', 'in_files')]),
	(MergeHemisNode, DataSinkNode, [('merged_file', 'nipype')])
])

Flow.write_graph(dotfilename=FlowName, graph2use='colored')
Flow.run('MultiProc', plugin_args={'n_procs': 12})

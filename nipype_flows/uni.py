import os, sys
import numpy as np
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.fsl import maths
from nipype.interfaces.fsl.utils import Merge
from nipype.interfaces.spm import Level1Design, EstimateModel, EstimateContrast, OneSampleTTestDesign, Threshold, Smooth
from nipype.interfaces.io import DataSink
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.algorithms.misc import Gunzip
from nipype import Workflow, Node
from nipype.pipeline.engine import MapNode, JoinNode

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import Paths, flow

FlowName = 'uni'
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
	Subjects = [str(sys.argv[1])]  # get subject code from input
TR = 1.206  # TR of the scans


# # 1 -- Subject Id

SubNode = Node(IdentityInterface(fields=['Subject']), name="SubNode")
SubNode.iterables = [('Subject', Subjects)]


# # # 2 -- Events -- # #

SubData = Function(
    input_names=['Subject'],
    output_names=['file_bold', 'file_mask', 'file_confounds', 'events_file'],
    function=Paths.subdata_glm)
SubDataNode = Node(SubData, name='SubData')

def subDM(events_file):
    import pandas as pd
    from nipype.interfaces.base import Bunch

    assert isinstance(events_file, list), "input not a list of files!"
    con_names = ['Encode_R', 'Delay_R', 'Response_R', 'Encode_U', 'Delay_U', 'Response_U']

    subject_info = []

    for f in events_file:
        e = pd.read_table(f)
        # repeating x task phase
        O_Er = e[(e.Phase == 1) & (e.IsLearned == 1)].onset.tolist()
        D_Er = e[(e.Phase == 1) & (e.IsLearned == 1)].duration.tolist()
        O_Dr = e[(e.Phase == 2) & (e.IsLearned == 1)].onset.tolist()
        D_Dr = e[(e.Phase == 2) & (e.IsLearned == 1)].duration.tolist()
        O_Rr = e[(e.Phase == 3) & (e.IsLearned == 1)].onset.tolist()
        D_Rr = e[(e.Phase == 3) & (e.IsLearned == 1)].duration.tolist()
        # unique x task phase
        O_Eu = e[(e.Phase == 1) & (e.IsLearned == 0)].onset.tolist()
        D_Eu = e[(e.Phase == 1) & (e.IsLearned == 0)].duration.tolist()
        O_Du = e[(e.Phase == 2) & (e.IsLearned == 0)].onset.tolist()
        D_Du = e[(e.Phase == 2) & (e.IsLearned == 0)].duration.tolist()
        O_Ru = e[(e.Phase == 3) & (e.IsLearned == 0)].onset.tolist()
        D_Ru = e[(e.Phase == 3) & (e.IsLearned == 0)].duration.tolist()

        si = Bunch(
            conditions=con_names,
            onsets=[O_Er, O_Dr, O_Rr, O_Eu, O_Du, O_Ru,],
            durations=[D_Er, D_Dr, D_Rr, D_Eu, D_Du, D_Ru]
            )
        subject_info.append(si)

    return subject_info  # this output will later be returned to infosource

SubDM = Function(
    input_names=['events_file'],
    output_names=['subject_info'],
    function=subDM)
SubDMNode = Node(SubDM, name='SubDM')

# In[3]:
# Mask
ApplyMaskNode = MapNode(maths.ApplyMask(output_type = 'NIFTI_GZ'),
    name = 'ApplyMask',
    iterfield=['in_file', 'mask_file']
    )
GZ_Node = MapNode(Gunzip(), name='Gunzip', iterfield=['in_file'])

# Design
SpecifyModel = SpecifySPMModel(
    concatenate_runs = True,
    input_units = 'secs',
    output_units = 'secs',
    time_repetition = TR,
    high_pass_filter_cutoff = 128)
SpecifyModelNode = Node(SpecifyModel, name='SpecifyModel')

L1_DM = Level1Design(
    bases={'hrf': {'derivs': [0, 0]}},
    timing_units = 'secs',
    interscan_interval = TR,
    model_serial_correlations = 'FAST'
)
L1_DMNode = Node(L1_DM, name = 'Level1Design')

# Estimate
L1_EstimateNode = Node(EstimateModel(estimation_method={'Classical': 1}), name='EstimateModel')

# Contrasts
L1_ContrastNode = Node(EstimateContrast(use_v8struct=True), name='L1_ContrastNode')

# Contrasts
con_names = ['Encode_R', 'Delay_R', 'Response_R', 'Encode_U', 'Delay_U', 'Response_U']
# repeating vs unique univariate contrasts for each task phase
c1 = ('Encode',    'T', con_names, [-1, 0, 0, 1, 0, 0]) 
c2 = ('Delay',     'T', con_names, [0, -1, 0, 0, 1, 0]) 
c3 = ('Response',  'T', con_names, [0, 0, -1, 0, 0, 1])
L1_ContrastNode.inputs.contrasts = [c1, c2, c3]
L1_ContrastNode.inputs.group_contrast = False

# Merge contrasts
MergeNode = Node(Merge(dimension='t', output_type='NIFTI_GZ'), name='Merge')

# Smooth
SmoothNode = Node(Smooth(), name='Smooth')
SmoothNode.inputs.fwhm = [4, 4, 4]

# Copy
DataSinkNode = Node(
    DataSink(
        base_directory = Folder['Base'],
        substitutions = [
            ('_ni/_Subject_', 'sub-'),
            ('/con_0', '/uni/con_0'),
            ('/scon_0', '/uni/scon_0'),
            ('spmT_0001_merged', 'spmT-UR')
            ]
        ),
    name='DataSink')


# # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])

Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])
Flow.connect([
    (SubNode, SubDataNode, [('Subject', 'Subject')]),
    (SubDataNode, SubDMNode, [('events_file', 'events_file')]),
    (SubDataNode, ApplyMaskNode, [
        ('file_bold', 'in_file'),
        ('file_mask', 'mask_file')
        ]),
    (ApplyMaskNode, GZ_Node, [('out_file', 'in_file')]),
    (SubDMNode, SpecifyModelNode, [('subject_info', 'subject_info')]),
    (SubDataNode, SpecifyModelNode, [('file_confounds', 'realignment_parameters')]),
    (GZ_Node, SpecifyModelNode, [('out_file', 'functional_runs')]),
    (SpecifyModelNode, L1_DMNode, [('session_info', 'session_info')]),
    (L1_DMNode, L1_EstimateNode, [('spm_mat_file', 'spm_mat_file')]),
    (L1_EstimateNode, L1_ContrastNode, [
        ('spm_mat_file', 'spm_mat_file'),
        ('residual_image', 'residual_image'),
        ('beta_images', 'beta_images')
        ]),
    (L1_ContrastNode, MergeNode, [('spmT_images', 'in_files')]),
    (MergeNode, DataSinkNode, [('merged_file', 'nipype')]),
    (L1_ContrastNode, DataSinkNode, [('con_images', 'nipype.@uni')]),
    (L1_ContrastNode, SmoothNode, [('con_images', 'in_files')]),
    (SmoothNode, DataSinkNode, [('smoothed_files', 'nipype.@smooth')]),
])

Flow.write_graph(graph2use='colored')
n_procs = 8 #len(Subjects)
Flow.run('MultiProc', plugin_args={'n_procs': n_procs})

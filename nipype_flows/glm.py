import os, sys
import numpy as np
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.fsl import maths
from nipype.interfaces.fsl.utils import Merge
from nipype.interfaces.spm import Level1Design, EstimateModel, EstimateContrast
from nipype.interfaces.io import DataSink
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.algorithms.misc import Gunzip
from nipype import Workflow, Node
from nipype.pipeline.engine import MapNode, JoinNode

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import Paths, utils

FlowName = 'glm'
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
    Subjects = [str(sys.argv[1])]  # get subject code from input

TR = 1.206  # TR of the scans

######################

SubNode = Node(IdentityInterface(fields=['Subject']), name='SubNode')
SubNode.iterables = [('Subject', Subjects)]


SubData = Function(
    input_names=['Subject'],
    output_names=['file_bold', 'file_mask', 'file_confounds', 'events_file'],
    function=Paths.subdata_glm)
SubDataNode = Node(SubData, name='SubData')

EventNumNode = Node(IdentityInterface(fields=['j']), name='EventNumNode')
EventNumNode.iterables = [('j', np.arange(99).tolist())]

SubDM = Function(
    input_names=['events_file', 'j'],
    output_names=['subject_info', 'contrasts'],
    function=utils.subDM)
SubDMNode = Node(SubDM, name='SubDM')

# Mask
ApplyMaskNode = MapNode(maths.ApplyMask(output_type = 'NIFTI_GZ'),
    name = 'ApplyMask',
    iterfield=['in_file', 'mask_file']
    )
GZ_Node = MapNode(Gunzip(), name='Gunzip', iterfield=['in_file'])


# Design
SpecifyModel = SpecifySPMModel(
    concatenate_runs=True,
    input_units='secs',
    output_units='secs',
    time_repetition=TR,
    high_pass_filter_cutoff=128)
SpecifyModelNode = Node(SpecifyModel, name='SpecifyModel')

L1_DM = Level1Design(
    bases={'hrf': {'derivs': [0, 0]}},
    timing_units='secs',
    interscan_interval=TR,
    model_serial_correlations='FAST'
)
L1_DMNode = Node(L1_DM, name = 'Level1Design')

# Estimate
L1_EstimateNode = Node(EstimateModel(estimation_method={'Classical': 1}), name='EstimateModel')

# Contrasts
L1_ContrastNode = Node(EstimateContrast(use_v8struct=True), name='L1_ContrastNode')
con = ['e', 'T', ['e'], [1]]
L1_ContrastNode.inputs.contrasts = [con]

# Merge individual GLMs
MergeGlmsNode = JoinNode(
    Merge(dimension='t', output_type='NIFTI_GZ'),
    joinsource='EventNumNode',
    joinfield='in_files',
    name='MergeGlms')

# Merge contrasts
MergeNode = Node(Merge(dimension='t', output_type='NIFTI_GZ'), name='Merge')

# Copy
DataSinkNode = Node(
    DataSink(
        base_directory = Folder['Base'],
        substitutions = [
            ('_ni/_Subject_', 'sub-'),
            ('spmT_0001_merged', 'spmT')
            ]
        ),
    name='DataSink')


# # # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])

Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])
Flow.connect([
    (SubNode, SubDataNode, [('Subject', 'Subject')]),
    (SubDataNode, ApplyMaskNode, [
        ('file_bold', 'in_file'),
        ('file_mask', 'mask_file')
        ]),
    (ApplyMaskNode, GZ_Node, [('out_file', 'in_file')]),
    (SubDataNode, SubDMNode, [('events_file', 'events_file')]),
    (EventNumNode, SubDMNode, [('j', 'j')]),
    (SubDMNode, SpecifyModelNode, [('subject_info', 'subject_info')]),
    (SubDataNode, SpecifyModelNode, [('file_confounds', 'realignment_parameters')]),
    (GZ_Node, SpecifyModelNode, [('out_file', 'functional_runs')]),
    (SpecifyModelNode, L1_DMNode, [('session_info', 'session_info')]),
    (L1_DMNode, L1_EstimateNode, [('spm_mat_file', 'spm_mat_file')]),
    (L1_EstimateNode, L1_ContrastNode, [
        ('spm_mat_file', 'spm_mat_file'),
        ('residual_image', 'residual_image'),
        ('beta_images', 'beta_images')
        ]),
    (L1_ContrastNode, MergeGlmsNode, [('spmT_images', 'in_files')]),
    (MergeGlmsNode, DataSinkNode, [('merged_file', 'nipype')])
])


Flow.write_graph(dotfilename=FlowName, graph2use='colored')
n_procs = 20 #len(Subjects)
Flow.run('MultiProc', plugin_args={'n_procs': n_procs})

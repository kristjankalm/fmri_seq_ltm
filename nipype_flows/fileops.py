import os, sys, shutil
from os.path import join as ojp
from nilearn._utils.glm import get_bids_files

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

sys.path.append(os.path.join(os.path.dirname(os.getcwd())))
from flow import Paths


Folder, _, _, Subjects = Paths.ExpData()

for Subject in Subjects:
    File, File = Paths.SubjectData(Subject)
    files_ = get_bids_files(Folder['Bids'], file_tag='phasediff', sub_label=Subject, modality_folder='fmap')
    for f_ in files_:
        if os.path.exists(f_):
            print(f_)
            os.remove(f_)
    
# Subjects = ['21']

# files = ['spmT.nii.gz', 'vois_aparc_2009.nii.gz', 'vois_aparc_2009.tsv']

# for s in Subjects:
# 	File, F = Paths.SubjectData(s)
# 	f_old = ojp(Folder['Base'], '_nipype', 'sub-' + s)
# 	if not os.path.exists(F['SubOut']):
# 		os.mkdir(F['SubOut'])
# 	for f in files:
# 		shutil.copyfile(ojp(f_old, f), ojp(F['SubOut'], f))

# files = ['SL.pkl', 'sl_rho_2.pkl',  'sl_rsa_1.nii.gz',  'sl_rsa_3.nii.gz', 'rsa_svm.csv',  'sl_rho_1.pkl',  'sl_rho_3.pkl',  'sl_rsa_2.nii.gz',  'vois_data.pkl']

# p = '1'
# m = 'H'

# for s in Subjects:
    
#     File, Folder = Paths.SubjectData(s)
    
#     files = ['H.csv', 'H-0_data.pkl', 'H-0_rdm.pkl', 'H-0-mask.nii.gz']
#     for f in files:
#         file = ojp(Folder['SubOut'], 'phase_' + p, f)
#         if os.path.exists(file):
#             print(file)
#             os.remove(file)
    
#     folder_1 = ojp(Folder['tmp'], 'rsa', '_Subject_' + s, '_model_' + m, '_phase_' + p, 'RsaData')
#     folder_2 = ojp(Folder['tmp'], 'rsa', '_Subject_' + s, '_model_' + m, '_phase_' + p, 'RsaVoi')
#     folder_2 = ojp(Folder['tmp'], 'rsa', '_Subject_' + s, '_model_' + m, '_phase_' + p, 'VoiMask')
#     # folder_ = ojp(Folder['tmp'], 'noiseceil')
#     folders = [folder_1, folder_2]
#     for folder_ in folders:
#         if os.path.exists(folder_):
#             print(folder_)
#             shutil.rmtree(folder_)
        
    
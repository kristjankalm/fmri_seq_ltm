import os
import sys
from nipype import Workflow, Node
from nipype.interfaces.io import DataSink
from nipype.interfaces.utility import Function, IdentityInterface

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import Paths, rsa

FlowName = 'rsa'
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
	Subjects = [str(sys.argv[1])]  # get subject code from input

# # 1 -- Phase

PhaseNumNode = Node(IdentityInterface(fields=['phase']), name="PhaseNumNode")
PhaseNumNode.iterables = [('phase', [1, 2, 3])]

# # 2 -- Seq Rep Models

ModelNode = Node(IdentityInterface(fields=['model']), name="ModelNode")
ModelNode.iterables = [('model', ['H', 'N2', 'IM', 'C'])]

# # 3 -- Subject Id

SubNode = Node(IdentityInterface(fields=['Subject']), name="SubNode")
SubNode.iterables = [('Subject', Subjects)]

# # 4 -- data RDMS -- # #

DataRDM = Function(
    input_names=['Subject'],
    output_names=['file_rdm'],
    function=rsa.data_rdm)
DataRDMNode = Node(DataRDM, name='DataRDM')

# # # 5 -- rsa model -- # #

RsaModel = Function(
    input_names=['Subject', 'file_rdm', 'phase', 'model'],
    output_names=['out_files'],
    function=rsa.model)
RsaModelNode = Node(RsaModel, name='RsaModel')

# # 6 -- anatomical vois -- # #

def voi_img(in_files):
    return in_files[0]

VoiImg = Function(
    input_names=['in_files'],
    output_names=['out_file'],
    function=voi_img)
VoiImgNode = Node(VoiImg, name='VoiImg')

VoiMask = Function(
    input_names=['Subject', 'img'],
    output_names=['file_mask'],
    function=rsa.mask)
VoiMaskNode = Node(VoiMask, name='VoiMask')

# 7 -- rsa data -- # #

RsaData = Function(
    input_names=['Subject', 'file_mask'],
    output_names=['files_out'],
    function=rsa.data)
RsaDataNode = Node(RsaData, name='RsaData')


# # 8 -- DataSink -- # #

sink_subs = [('_ni/_Subject_', 'sub-'), ('_phase', 'phase'), ('_model_H', ''),
            ('_model_N2', ''), ('_model_IM', ''), ('_model_C', '')]

DataSinkNode = Node(
    DataSink(base_directory=Folder['Base'], substitutions=sink_subs),
    name='DataSink')

# # # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])
Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])

Flow.connect([
    (SubNode, DataRDMNode, [('Subject', 'Subject')]),
    (DataRDMNode, DataSinkNode, [('file_rdm', 'nipype.@file_rdm')]),
    (DataRDMNode, RsaModelNode, [('file_rdm', 'file_rdm')]),
    (SubNode, RsaModelNode, [('Subject', 'Subject')]),
    (PhaseNumNode, RsaModelNode, [('phase', 'phase')]),
    (ModelNode, RsaModelNode, [('model', 'model')]),
    (RsaModelNode, DataSinkNode, [('out_files', 'nipype.@out_files')]),
    (RsaModelNode, VoiImgNode, [('out_files', 'in_files')]),
    (SubNode, VoiMaskNode, [('Subject', 'Subject')]),
    (VoiImgNode, VoiMaskNode, [('out_file', 'img')]),
    (VoiMaskNode, DataSinkNode, [('file_mask', 'nipype.@file_mask')]),
    (SubNode, RsaDataNode, [('Subject', 'Subject')]), # data
    (VoiMaskNode, RsaDataNode, [('file_mask', 'file_mask')]),
    (RsaDataNode, DataSinkNode, [('files_out', 'nipype.@RsaData')]),
])

Flow.write_graph(graph2use='colored')
n_procs = len(Subjects)
Flow.run('MultiProc', plugin_args={'n_procs': n_procs})

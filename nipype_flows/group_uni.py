import os, sys
import numpy as np
from nipype.interfaces.utility import Function, IdentityInterface
from nipype.interfaces.fsl import maths
from nipype.interfaces.fsl.utils import Merge
from nipype.interfaces.spm import Level1Design, EstimateModel, EstimateContrast, OneSampleTTestDesign, Threshold, Smooth
from nipype.interfaces.io import DataSink, SelectFiles
from nipype.algorithms.modelgen import SpecifySPMModel
from nipype.algorithms.misc import Gunzip
from nipype import Workflow, Node
from nipype.pipeline.engine import MapNode, JoinNode

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from flow import Paths, flow

FlowName = 'group-uni'
Folder, _, _, Subjects = Paths.ExpData()

if len(sys.argv) > 1:
	Subjects = [str(sys.argv[1])]  # get subject code from input

# # 1 -- Task Phase (Encode, Delay, Recall)
PhaseNumNode = Node(IdentityInterface(fields=['phase']), name="PhaseNumNode")
PhaseNumNode.iterables = [('phase', ['1', '2', '3'])]

# # 2 --  get contrast files from all subjects per phase
templates = {'cons': os.path.join(Folder['Out'], 'sub-*', 'uni/scon_000{phase}.nii')}
SelectFilesNode = Node(SelectFiles(templates), name='SelectFiles')
# o = SelectFiles(templates).run() # this has to be done to the function, not the node
# print(len(o.outputs.cons))

# # 3 --  One Sample T-Test Design - creates one sample T-Test Design
TtestNode = Node(OneSampleTTestDesign(), name='Ttest')

# # 4 --  EstimateModel - estimate the parameters of the model
GroupEstNode = Node(EstimateModel(estimation_method={'Classical': 1}), name='GroupEst')

# # 5 --  EstimateContrast - estimates simple group contrast
GroupConNode = Node(EstimateContrast(group_contrast=True), name='GroupCon')
c1 = ('Group', 'T', ['mean'], [1])
GroupConNode.inputs.contrasts = [c1]

# # 6 --  Threshold (multiple comparisons) for contrasts
ThresholdNode = Node(
    Threshold(
        contrast_index = 1,
        use_topo_fdr = True,
        use_fwe_correction = False,
        extent_threshold = 0,
        height_threshold = 0.005,
        height_threshold_type = 'p-value',
        extent_fdr_p_threshold = 0.05,
    ),
    name = 'Threshold')

# # Copy
DataSinkNode = Node(
    DataSink(
        base_directory = Folder['Base'],
        substitutions = [
            ('_ni/_phase_', ''),
            ]
        ),
    name='DataSink')

# # # FLOW # #

print('Changing to: ' + Folder['tmp'])
os.chdir(Folder['tmp'])

Flow = Workflow(name=FlowName, base_dir=Folder['tmp'])
Flow.connect([
    (PhaseNumNode, SelectFilesNode, [('phase', 'phase')]),
    (SelectFilesNode, TtestNode, [('cons', 'in_files')]),
    (TtestNode, GroupEstNode, [('spm_mat_file', 'spm_mat_file')]),
    (GroupEstNode, GroupConNode, [
        ('spm_mat_file', 'spm_mat_file'),
        ('residual_image', 'residual_image'),
        ('beta_images', 'beta_images')
        ]),
    (GroupConNode, ThresholdNode, [
        ('spm_mat_file', 'spm_mat_file'),
        ('spmT_images', 'stat_image'),
        ]),
    (ThresholdNode, DataSinkNode, [('thresholded_map', 'nipype.group')]),
    (GroupConNode, DataSinkNode, [('spmT_images', 'nipype.group.@con')]),
])

#Flow.write_graph(graph2use='colored')
n_procs = 6 #len(Subjects)
Flow.run('MultiProc', plugin_args={'n_procs': n_procs})

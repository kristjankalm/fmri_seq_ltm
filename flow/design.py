import pandas as pd
import numpy as np
from flow import Paths

def get_filter(F, labels, verbose=True):
    F_false = np.zeros((len(labels), len(F)))
    f = np.array(F_false, dtype=bool)
    for i, kv in enumerate(F.items()):
        if verbose:
            print(i, kv[0], kv[1])
        f[:, i] = labels[kv[0]].isin(kv[1])
    Filter = ~((f == False).any(1)).astype(bool)
    # print(len(Filter[Filter == True]))
    return Filter


def get_design(FilterDict, Subject='01', LabelName='SeqId', verbose=True):
    
    from nilearn._utils.glm import get_bids_files
    
    File, _ = Paths.SubjectData(Subject)
    Tsv = File['Tsv']
    labels = pd.read_csv(Tsv[0], sep="\t")
    filter_excl_rest = labels['Phase'].isin([1, 2, 3])  # exclude rest events
    labels = labels[filter_excl_rest]
    # print(labels)
    Filter = get_filter(FilterDict, labels, verbose=False)
    try:
        Labels = labels[LabelName]
    except KeyError:
        print('No such label name: ' + LabelName )
        raise KeyError()
    Labels = Labels[Filter]
    if verbose:
        print('Filter: ' + str(len(Filter[Filter == True])), end='\r')
    assert len(Filter[Filter == True]) == len(Labels), 'Filter and Labels sizes do not match'
    return Filter, Labels


def get_RDM(Model='H'):
    import scipy.io
    import os
    Folder, _, _, _ = Paths.ExpData()
    fileRDM = os.path.join(Folder['csv'], 'rdm_stim_phase.mat')
    MatRDM = scipy.io.loadmat(fileRDM)
    #RDM = [MatRDM['H'], MatRDM['N2'], MatRDM['N3'], MatRDM['IM'], MatRDM['C']]
    return MatRDM[Model]

def create_RDM():
    return 0
    
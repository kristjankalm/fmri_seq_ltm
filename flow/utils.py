import pandas as pd
import numpy as np
from os.path import join as opj
import os
from nipype.interfaces.base import Bunch


def get_confounds(confounds_file):
    assert isinstance(confounds_file, list), "input not a list of files!"
    cfiles = []
    for f in confounds_file:
        confounds_in = pd.read_csv(f, sep="\t")
        move_cols = ['X', 'Y', 'Z', 'RotX', 'RotY', 'RotZ']
        try:
            confounds_in = confounds_in[move_cols]
        except KeyError:
            print('No match for columns, trying alternative labels ')
            move_cols = ['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']
            confounds_in = confounds_in[move_cols]
        confounds = np.array(confounds_in)
        # if np.sum(confounds == 'n/a') > 0:
        # 	print('exist na')
        # 	confounds[confounds == 'n/a'] = 0
        confounds = confounds.astype(float)
        file_path = os.path.dirname(f)
        file_name = os.path.splitext(f)[0]
        confoundsfile = opj(file_path, file_name + '_move' + '.tsv')
        np.savetxt(confoundsfile, np.array(confounds), '%5.3f')
        cfiles.append(confoundsfile)
    return cfiles


def get_contrasts_uni():
    # Condition names
    condition_names = ['Encode', 'Delay', 'Response']
    # Contrasts
    cont01 = ['average', 'T', condition_names, [1/2., 1/2., 1/2.]]
    cont02 = ['Encode', 'T', condition_names, [1, 0, 0]]
    cont03 = ['Delay', 'T', condition_names, [0, 1, 0]]
    cont04 = ['Response', 'T', condition_names, [0, 0, 1]]
    cont05 = ['Response > Encode', 'T', condition_names, [-1, 0, 1]]
    cont06 = ['Delay > Encode', 'T', condition_names, [-1, 1, 0]]
    cont07 = ['activation', 'F', [cont02, cont03, cont04]]
    contrast_list = [cont01, cont02, cont03, cont04, cont05, cont06, cont07]
    return contrast_list

def subDM(events_file, j: int):
    from flow.utils import get_subject_info_glm
    subject_info = get_subject_info_glm(events_file, j)
    num_con = len(subject_info[0].conditions)
    print(subject_info)
    print('num_con: ' + str(num_con))
    contrasts = []
    return subject_info, contrasts

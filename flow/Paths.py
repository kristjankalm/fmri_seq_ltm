import pandas as pd
import os
from os.path import join as ojp

def ExpData(fig_subfolder=''):

    # change the following two folders per your filesystem
    Folder = {'home': os.environ.get('HOME')}
    data_cbu = '/imaging/norris/users/kk02/data'
    Folder['Base'] = data_cbu + '/f/_ni'  # base folder for all the generated outputs
    Folder['Bids'] = data_cbu + '/f/bids'  # location of the BIDS dataset
    Folder['Fig'] = ojp(Folder['home'], '_pubs/seq_plos_cb/fig/source') # export folder for figures
        
    # these are not required to be changed
    Folder['tmp'] = ojp(Folder['Base'], 'tmp')
    Folder['Out'] = ojp(Folder['Base'], 'nipype')
    Folder['csv'] = ojp(Folder['Out'], 'csv')
    Folder['Fprep'] = ojp(Folder['Base'], 'fmriprep')
    Folder['Wf'] = ojp(Folder['tmp'], 'fmriprep_wf')
    Folder['Fs'] = ojp(Folder['Base'], 'freesurfer')
    Folder['SourceData'] = ojp(Folder['Bids'], 'sourcedata')

    FileSubjects = ojp(Folder['Bids'], 'participants.tsv')
    SubData = pd.read_csv(FileSubjects, sep="\t")
    SubInclude = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21]
    SubFilter = [i+1 in SubInclude for i in range(SubData.shape[0])]
    Subjects = [str(s).zfill(2) for s in SubInclude]
    
    # export sub-folder for figures, if defined
    if fig_subfolder:
        Folder['Fig'] = ojp(Folder['Fig'], fig_subfolder)
        if not os.path.exists(Folder['Fig']):
            print('Creating : ' + Folder['Fig'])
            os.mkdir(Folder['Fig'])
        print('Figures export: ' + Folder['Fig'])

    return Folder, SubData, SubFilter, Subjects

def SubjectData(Subject: str):

    from nilearn._utils.glm import get_bids_files

    Folder, _, _, _ = ExpData()
    SubjectFolder = 'sub-' + Subject

    Folder['FsMri'] = ojp(Folder['Fs'], SubjectFolder, 'mri')
    Folder['FsVol'] = ojp(Folder['Fs'], SubjectFolder, '_vol')
    Folder['FsLabel'] = ojp(Folder['Fs'], SubjectFolder, '_label_2009')
    Folder['SubOut'] = ojp(Folder['Out'], SubjectFolder)
    Folder['SubTmp'] = ojp(Folder['tmp'], 'voi', '_Subject_' + Subject)

    # fprep discrepancy btw versions: bold, confounds, mask, T1w

    File = {
        #'TaskTrials': ojp(Folder['Task'], 'T_' + Subject + '.mat'),
        #'TaskPractice': ojp(Folder['Task'], 'P_' + Subject + '.mat'),
        #'Task': ojp(Folder['Task'], 'task_' + Subject + '.tsv'),        
        'Bold': get_bids_files(Folder['Fprep'], file_tag='bold', sub_label=Subject, modality_folder='func', file_type='nii.gz'),
        'Mask': get_bids_files(Folder['Fprep'], file_tag='mask', sub_label=Subject, modality_folder='func', file_type='nii.gz'),
        'Confounds': get_bids_files(Folder['Fprep'], file_tag='regressors', sub_label=Subject, modality_folder='func', file_type='tsv'),
        'Tsv': get_bids_files(Folder['Bids'], file_tag='events', sub_label=Subject, file_type='tsv'),
        'Json': get_bids_files(Folder['Bids'], file_tag='bold', sub_label=Subject, file_type='json'),
        # 'SegT1w': ojp(Folder['Fprep'], Subject, 'anat', Subject + '_T1w_label-aseg_roi.nii.gz'),
        # 'MNI': ojp(Folder['Fprep'], Subject, 'anat', Subject + '_T1w_space-MNI152NLin2009cAsym_preproc.nii.gz'),
        'Roi': get_bids_files(Folder['Fprep'], file_tag='label-A_roi', sub_label=Subject, modality_folder='anat', file_type='nii.gz'),
        'MNI': get_bids_files(Folder['Fprep'], file_tag='space-MNI152NLin2009cAsym_desc-preproc_T1w', sub_label=Subject, modality_folder='anat', file_type='nii.gz'),
        'T1w': get_bids_files(Folder['Fprep'], file_tag='desc-preproc_T1w', sub_label=Subject, modality_folder='anat', file_type='nii.gz'),
        # SPM
        'spmT': ojp(Folder['SubOut'], 'spmT.nii.gz'),
        'dataRDM': ojp(Folder['SubOut'], 'data_rdm.pkl'),
        # ANTS
        'RefMNI': ojp(Folder['Wf'], 'single_subject_' + Subject + '_wf/func_preproc_task_gabor_run_01_wf/bold_mni_trans_wf/gen_ref/1mm_T1_reference.nii.gz'),
        'TransAffine': ojp(Folder['Wf'], 'single_subject_' + Subject + '_wf/func_preproc_task_gabor_run_01_wf/bold_reg_wf/bbreg_wf/fsl2itk_fwd/affine.txt'),
        'TransMNI': ojp(Folder['Wf'], 'single_subject_' + Subject + '_wf/anat_preproc_wf/t1_2_mni/ants_t1_to_mniComposite.h5'),
        # VOI
        'TsvAparc': ojp(Folder['SubOut'], 'vois_aparc.tsv'),
        'TsvAparc_2009': ojp(Folder['SubOut'], 'vois_aparc_2009.tsv'),
        'Aparc': ojp(Folder['SubOut'], 'vois_aparc.nii.gz'),
        'Aparc_2009': ojp(Folder['SubOut'], 'vois_aparc_2009.nii.gz'),
        'VoisData': ojp(Folder['SubOut'], 'vois_data.pkl'),
        'VoisRDM': ojp(Folder['SubOut'], 'vois_rdm.pkl'),
        # FREESURFER
        'FsMriT1': ojp(Folder['FsMri'], 'T1.mgz')
    }

    # resolve discrepancies btw fmriprep versions
    if not File['Bold']:
        File['Bold'] = get_bids_files(Folder['Fprep'], file_tag='preproc', sub_label=Subject, modality_folder='func', file_type='nii.gz')
    if not File['Mask']:
        File['Mask'] = get_bids_files(Folder['Fprep'], file_tag='brainmask', sub_label=Subject, modality_folder='func', file_type='nii.gz')
    if not File['Confounds']:
        File['Confounds'] = get_bids_files(Folder['Fprep'], file_tag='confounds', sub_label=Subject, modality_folder='func', file_type='tsv')
    if not File['MNI']:
        File['MNI'] = get_bids_files(Folder['Fprep'], file_tag='space-MNI152NLin2009cAsym_preproc', sub_label=Subject, modality_folder='anat', file_type='nii.gz')
    if not File['T1w']:
        File['T1w'] = get_bids_files(Folder['Fprep'], file_tag='T1w_preproc', sub_label=Subject, modality_folder='anat', file_type='nii.gz')

    return File, Folder


def subdata_label(Subject: str):
    import os
    from flow.Paths import SubjectData

    File, Folder = SubjectData(Subject)
    subject_folder = 'sub-' + Subject
    folder_fs = Folder['Fs']
    folder_fslabel = Folder['FsLabel']
    file_FsMriT1 = File['FsMriT1']
    file_ref_MNI = File['RefMNI']
    file_trans_MNI = File['TransMNI']
    file_trans_affine = File['TransAffine']
    files_trans = [file_trans_MNI, file_trans_affine]

    # create folders
    if not os.path.exists(folder_fslabel):
        print('Creating: ' + folder_fslabel)
        os.mkdir(folder_fslabel)

    return subject_folder, folder_fs, folder_fslabel, file_FsMriT1, file_ref_MNI, files_trans


def subdata_voi(Subject: str):
    import os
    from flow.Paths import SubjectData
    from flow.voi import get_tsv
    import pandas as pd

    File, Folder = SubjectData(Subject)

    file_vois_aparc_2009 = File['Aparc_2009']
    file_mask = File['Mask']
    if type(file_mask) is list:
        file_mask = file_mask[0]
    file_spmT = File['spmT']

    # tsv_vois
    if os.path.exists(File['TsvAparc_2009']):
        print(File['TsvAparc_2009'])
    else:
        T = pd.concat([get_tsv('lh', Folder['FsLabel']), get_tsv('rh', Folder['FsLabel'])], axis=0, ignore_index=True)
        T.to_csv(File['TsvAparc_2009'], sep='\t')
    tsv_vois_aparc_2009 = File['TsvAparc_2009']

    return file_vois_aparc_2009, tsv_vois_aparc_2009, file_mask, file_spmT


def subdata_glm(Subject: str):
    from flow.Paths import SubjectData
    from flow.utils import get_confounds  # , get_subject_info_lda, get_contrasts_lda
    from nilearn._utils.glm import get_bids_files

    File, Folder = SubjectData(Subject)

    file_bold = File['Bold']
    file_mask = File['Mask']
    # if type(file_mask) is list:
    # 	file_mask = file_mask[0]
    print(File['Confounds'])
    file_confounds = get_confounds(File['Confounds'])
    events_file = get_bids_files(Folder['Bids'], sub_label=Subject, file_type='tsv')

    # print(file_bold, file_mask, file_confounds, events_file)

    return file_bold, file_mask, file_confounds, events_file

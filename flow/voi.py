import pandas as pd
import numpy as np
from os.path import join as ojp

def annot2label(Subject, hemi, folder_fs, folder_fslabel):
    import os, subprocess
    import pandas as pd
    import numpy as np
    from os.path import join as ojp
        
    subject_folder = 'sub-' + Subject
    FolderLabels = os.path.join(folder_fslabel, hemi)

    # create folders
    if not os.path.exists(FolderLabels):
        print('Creating : ' + FolderLabels)
        os.mkdir(FolderLabels)

    cmd_label2vol = 'mri_annotation2label --subject ' + subject_folder + ' --annotation ' + \
        'aparc.a2009s --hemi ' + hemi + ' --outdir ' + FolderLabels
    print(cmd_label2vol)
    subprocess.check_output([cmd_label2vol], shell=True)

    # get filenames from folder
    FilesLabel = [os.path.join(FolderLabels, f) for f in os.listdir(FolderLabels) if os.path.isfile(os.path.join(FolderLabels, f))]
    FilesLabel.sort()

    # rename files which have '&' in filename
    for f in FilesLabel:
        newF = f.replace('&', '_')
        os.rename(f, newF)

    FilesLabel = [os.path.join(FolderLabels, f) for f in os.listdir(FolderLabels) if os.path.isfile(os.path.join(FolderLabels, f))]
    FilesLabel.sort()

    # write filenames to tsv
    FileRoiTsv = os.path.join(folder_fslabel, 'rois_' + hemi + '.tsv')
    print('Write sorted vois to csv: ' + FileRoiTsv)
    df = pd.DataFrame(data=FilesLabel, columns=['RoiFile'])
    df.to_csv(FileRoiTsv, index=False)

    # return list of label files
    return FilesLabel, FileRoiTsv


def get_data_voi(Subject):

    import os
    import pandas as pd
    import numpy as np
    from nilearn.image import load_img, index_img
    from nilearn.input_data import NiftiMasker
    from flow import Paths

    file_vois_aparc_2009, tsv_vois_aparc_2009, _, file_spmT = Paths.subdata_voi(Subject)

    pickle_name = 'vois_data.pkl'
    file_pickle = os.path.join(os.getcwd(), pickle_name)
    print('Creating ' + file_pickle + ' ...')
    
    T = pd.read_table(tsv_vois_aparc_2009)
    VoiImg = load_img(file_vois_aparc_2009)
    assert VoiImg.shape[3] == T.shape[0], 'Length of images and labels dont match'

    V = np.arange(0, T.shape[0])
    z = []
    for i in V:
        print(T['RoiNames'][i])
        Mask = index_img(file_vois_aparc_2009, i)
        if Mask.get_data().sum():
            Masker = NiftiMasker(mask_img=Mask, standardize=True)
            data = Masker.fit_transform(file_spmT)
        else:
            data = []
        z.append(data)
    d = {'RoiNames': T['RoiNames'][V], 'Data': z}
    df = pd.DataFrame(data=d)
    df.to_pickle(file_pickle)
    return file_pickle
# import pandas as pd
# import numpy as np
# from os.path import join as ojp

def data(Subject, distance_measure='correlation'):

    import os
    import pandas as pd
    import numpy as np
    from nilearn.image import load_img, index_img
    from nilearn.input_data import NiftiMasker
    from flow import Paths
    from scipy.spatial.distance import cdist
    from nilearn import signal
    from stats.matrix import upper_tri_vector

    file_vois_aparc_2009, tsv_vois_aparc_2009, _, file_spmT = Paths.subdata_voi(Subject)
    T = pd.read_table(tsv_vois_aparc_2009)
    # T = T[T.index==25] # debug 

    z = []
    u = []
    
    for i in T.index:
        print(str(i) + ' ' + T['RoiNames'][i], end='\r')   
        
        # masked data
        Mask = index_img(file_vois_aparc_2009, i)
        if Mask.get_data().sum():
            Masker = NiftiMasker(mask_img=Mask, standardize=True)
            data = Masker.fit_transform(file_spmT)
            # rdm
            F = data
            FilterZero = F[0][:] != 0  # filter out zero-columns
            F = F[:][:, FilterZero]
            #F = signal.clean(F)
            rdm = cdist(F, F, distance_measure)
            u_rdm = upper_tri_vector(rdm)
        else:
            data = []
            u_rdm = []
        z.append(data)
        u.append(u_rdm)
        
    D = {'RoiNames': T['RoiNames'], 'Data': z}
    df_d = pd.DataFrame(data=D)
    
    file_pickle = os.path.join(os.getcwd(), 'vois_data.pkl')
    df_d.to_pickle(file_pickle)
    
    U = {'RoiNames': T['RoiNames'], 'Data': u}
    df_u = pd.DataFrame(data=U)
    
    file_pickle_u = os.path.join(os.getcwd(), 'vois_rdm.pkl')
    df_u.to_pickle(file_pickle_u)
    
    files_out = [file_pickle, file_pickle_u]
    
    return files_out


def rdms(Subjects: list, Vois: list, Filter: list, phase=1, model='H'):
    # Vois = [25, 66]
    # Subjects = ['13', '01']
    # Filter = [True, False, ..., False]
    
    import os
    import pandas as pd
    import numpy as np
    from flow import Paths
    from scipy.spatial.distance import cdist, squareform
    from scipy.stats import rankdata
    from nilearn import signal
    
    n_sub = len(Subjects)
    n_voi = len(Vois)
    n_lab = np.sum(Filter)
    r_rows, r_cols = np.triu_indices(n_lab, 1)  # indices for the upper triangle of the matrix
    n_rdm = r_rows.shape[0]

    R_rho = np.zeros((n_rdm, n_sub, n_voi))  # spearman correlation
    R_rnk = np.zeros((n_rdm, n_sub, n_voi))  # rank correlation
    R_f = [[]] * n_sub

    for s, Subject in enumerate(Subjects):
        
        print('Subject: ' + Subject, end='\r')
        File, Folder = Paths.SubjectData(Subject)
        
        file_pickle_rdm = os.path.join(Folder['SubOut'], 'phase_' + phase, model + '-0-0_rdm.pkl')
        data_rdm = pd.read_pickle(file_pickle_rdm)
        
        file_pickle_data = os.path.join(Folder['SubOut'], 'phase_' + phase, model + '-0-0_data.pkl')
        data_data = pd.read_pickle(file_pickle_data)
        
        r_v = [[]] *  n_voi
        for j, v in enumerate(Vois):
            F = data_rdm.loc[[v]]['Data'].values[0]
            Y = data_data.loc[[v]]['Data'].values[0]
            if len(F) > 1:
                rdm = squareform(F)
                rdm = rdm[Filter][:,Filter]

                FilterZero = Y[0][:] != 0
                #f = Y[Filter][:, FilterZero]
                f = Y[Filter][:]
                
                rdm = cdist(f, f, 'correlation')
                u_tri = rdm[r_rows, r_cols]
            else:
                u_tri = np.full(n_rdm, np.nan)
                f = None

            R_rho[:, s, j] = u_tri
            R_rnk[:, s, j] = rankdata(u_tri)
            r_v[v] = f
            
        R_f[s] = r_v

    return R_rho, R_rnk, R_f


def ceiling(R_rnk, Subjects, Vois):
    
    import warnings, os
    import pandas as pd
    import numpy as np
    from flow import Paths
    from scipy.spatial.distance import cdist, squareform
    from scipy.stats import rankdata, spearmanr, ttest_1samp
    
    n_sub = len(Subjects)
    n_voi = len(Vois)
    n_rdm = R_rnk.shape[0]

    RDM_avg = np.zeros((n_rdm, n_voi))  # avg RDM across subj per region
    RDM_LB = np.full((n_sub, n_voi), np.nan)  # noise ceiling lower bound; nan array
    RDM_UB = np.full((n_sub, n_voi), np.nan)  # noise ceiling upper bound; nan array

    warnings.filterwarnings('ignore')  # ignore scipy spearman correlation warning

    for v in range(n_voi):
        RDM_avg[:, v] = np.nanmean(R_rnk[:, :, v], axis=1)  # avg RDM across subj for region
        for s in range(n_sub):
            RDM_subj = R_rnk[:, s, v]  # subj's RDM
            ff = [sb not in [s] for sb in range(n_sub)]  # col index with current subject == False
            RDM_avg_wo = np.nanmean(R_rnk[:, ff, v], axis=1)  # avg RDM *without* subj's RDM
            RDM_LB[s, v], _ = spearmanr(RDM_subj, RDM_avg_wo)
            RDM_UB[s, v], _ = spearmanr(RDM_subj, RDM_avg[:, v])

    RDM_LB_voi = np.nanmean(RDM_LB, axis=0)  # avg LB for all regions
    RDM_UB_voi = np.nanmean(RDM_UB, axis=0)  # avg UB

    # lower bound significance
    aR = np.arctanh(RDM_LB)  # fisher transform
    t_ = ttest_1samp(aR, 0, axis=0, nan_policy='omit')
    RDM_LB_p = np.array(np.round(t_.pvalue, 6))

    return RDM_LB_voi, RDM_UB_voi, RDM_avg, RDM_LB_p


def fit(R_rho, R_f, Models, Subjects, Vois, Label):
    
    import numpy as np
    from scipy.spatial.distance import cdist, squareform
    from scipy.stats import rankdata, spearmanr, ttest_1samp
    from stats.test import binary_regression, binary_r
    
    n_sub = len(Subjects)
    n_voi = len(Vois)
    n_mod = len(Models)
    r_rows, r_cols = np.triu_indices(Models[0].shape[0], 1)
    n_rdm = r_rows.shape[0]

    # Model RDMs into matrix of upper triangle values
    MRDM = np.zeros((n_rdm, n_mod))
    for m, model in enumerate(Models):
        MRDM[:, m] = model[r_rows, r_cols]

    # Model fits to each region's average subjects' RDM
    C = np.full((n_sub, n_mod, n_voi), np.nan)
    for v in range(n_voi):
        for m in range(n_mod):
            binary_ranks = np.unique(MRDM[:, m]).shape[0] == 2
            for s in range(n_sub):
                C[s, m, v], _ = spearmanr(MRDM[:, m], R_rho[:, s, v])
                if binary_ranks:
                    C[s, m, v] = binary_r(Label, R_f[s][v])
    RDM_fit = np.nanmean(C, axis=0)
    RDM_ebar = np.nanstd(C, axis=0) / np.sqrt(n_sub - 1) # SEM
    
    ttest = ttest_1samp(np.arctanh(C), popmean=0, nan_policy='omit')
    p = np.array(np.round(ttest.pvalue, 6))
    
    RDM_fit = np.array(np.round(RDM_fit, 6))
    RDM_ebar = np.array(np.round(RDM_ebar, 6))

    return RDM_fit, RDM_ebar, ttest.statistic, p, C


def mds(M, V, n=10**2, metric=True):
    from sklearn.manifold import MDS
    ss = np.zeros(n)
    for m in np.arange(n):
        model_voxel = MDS(n_components=2, dissimilarity='precomputed', random_state=m, metric=metric)
        Yv = model_voxel.fit_transform(V)
        ss[m] = np.linalg.norm(M - Yv)
    print(ss.min())
    im = np.argmax(-ss)
    print(im)
    return im


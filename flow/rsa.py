def data_rdm(Subject):
    
    import os
    from flow import Paths
    from nilearn.image import load_img
    import pickle
    from sl.searchlight import SearchLight as sl
    
    File, Folder = Paths.SubjectData(Subject)
    file_mask = File['Mask']
    if type(file_mask) is list:
        file_mask = file_mask[0]

    mask = load_img(file_mask).get_data()
    data = load_img(File['spmT']).get_data()
    assert data.shape[:3] == mask.shape[:3], 'Data and mask sizes do not match'
    
    # # initiate data RDMs class (radius is in voxels)
    dataRDM = sl(mask, radius=3, thr=1.0)
    # compute RDMs for data
    dataRDM.fit_rdm(data)

    file_rdm = os.path.join(os.getcwd(), 'data_rdm.pkl')
    print('Saving data RDMs: ' + file_rdm, end='\r')
    pickle.dump(dataRDM, open(file_rdm, 'wb'))
    
    return file_rdm


def model(Subject, file_rdm, phase=1, model='H'):

    import os, pickle
    import numpy as np
    from nilearn.image import new_img_like
    from flow import Paths
    from flow.design import get_design, get_RDM
    from flow.rsa import mask as rsa_mask
    
    File, Folder = Paths.SubjectData(Subject)

    print('Loading data RDMs: ' + file_rdm, end='\r')
    dataRDM = pickle.load(open(file_rdm, 'rb'))

    # RSA
    RDM = get_RDM(Model=model)

    out_files = []
    Analyses = [[[0], 'SeqId'], [[0,1], 'IsLearned'], [[1], 'SeqId']]

    for a in Analyses:

        islearned = a[0]
        labelname = a[1]
        islearned_str = ''.join([str(l) for l in islearned])

        # define filter
        FilterDict = {'Phase': [phase], 'IsLearned': islearned} 
        Filter, Label = get_design(FilterDict, LabelName=labelname)

        # run RSA
        if np.unique(Label).shape[0] == 2:
            dataRDM.rsa(Filter, RDM, labels=Label)
        else:
            dataRDM.rsa(Filter, RDM)
        
        # write img
        img = new_img_like(File['Mask'], dataRDM.R)
        out_file = os.path.join(os.getcwd(), model + '-' + islearned_str + '.nii.gz')
        print('Saving: ' + out_file)
        img.to_filename(out_file)
        
        out_files.append(out_file)
    
    return out_files

def mask(Subject, img):
    
    import os
    import pandas as pd
    import numpy as np
    from scipy.stats import norm
    from stats.rsa import spearman_to_z
    from nilearn.image import load_img, index_img, concat_imgs
    from nilearn.input_data import NiftiMasker
    from flow import Paths
    
    File, Folder = Paths.SubjectData(Subject)

    norm_thresh = 10**-2
    norm_thresh_label = f'{norm_thresh:.4f}'
    do_b = False
    
    file_vois_aparc_2009, tsv_vois_aparc_2009, _, file_spmT = Paths.subdata_voi(Subject)

    T = pd.read_table(tsv_vois_aparc_2009, index_col=0)
    VoiImg = load_img(file_vois_aparc_2009)
    assert VoiImg.shape[3] == T.shape[0], 'Length of images and labels dont match'

    Masks = []

    for i in T.index:

        print(str(i) + ' ' + T['RoiNames'][i], end='\r')
        Mask = index_img(file_vois_aparc_2009, i)
        Masker = NiftiMasker(mask_img=Mask, standardize=True)
        data = Masker.fit_transform(img)
        data = np.array(data).flatten()

        # which voxels > z_thresh
        z_thresh = norm.ppf(1-(norm_thresh))
        if do_b:
            z_thresh = norm.ppf(1-(norm_thresh/data.shape[0])) 
            norm_thresh_label = 'b' + norm_thresh_label
            
        is_sig = spearman_to_z(data) > z_thresh
        # print('Num z voxels: ' + str(np.where(is_sig)[0].shape[0]) )

        # create new binary mask 
        sig_int = np.array(is_sig, dtype=int)
        m_img = Masker.inverse_transform(sig_int)
        Masks.append(m_img)
        
    ImgMasks = concat_imgs(Masks)
    
    _, tail = os.path.split(img)
    file_mask = os.path.join(os.getcwd(), tail.split('.')[0] + '-' + norm_thresh_label +'.nii.gz') 
    
    ImgMasks.to_filename(file_mask)
    
    return file_mask

def data(Subject, file_mask):
    
    import os
    import pandas as pd
    import numpy as np
    from nilearn.image import load_img, index_img
    from nilearn.input_data import NiftiMasker
    from scipy.spatial.distance import cdist
    from nilearn import signal
    from stats.matrix import upper_tri_vector
    from flow import Paths

    File, Folder = Paths.SubjectData(Subject)
    
    V = pd.read_table(File['TsvAparc_2009'], index_col=0)
    #V = V[V.index == 25]

    # fmri_img = index_img(File['spmT'], Filter)
    fmri_img = load_img(File['spmT'])
    mask_img = load_img(File['Mask'][0])
    
    d = [] # voxel data
    u = [] # upper tri of rdm 
    
    for voi in V.index:
        
        print(str(voi) + ' ' + V['RoiNames'], end='\r')

        th_img = index_img(file_mask, voi)
        num_vox = th_img.get_data().sum()
    
        if num_vox > 30:
            Masker = NiftiMasker(mask_img=th_img, standardize=True)     
            data = Masker.fit_transform(fmri_img)
            # rdm
            F = data
            FilterZero = F[0][:] != 0  # filter out zero-columns
            F = F[:][:, FilterZero]        
            F = signal.clean(F)
            rdm = cdist(F, F, 'correlation')
            u_rdm = upper_tri_vector(rdm)
        else:
            data = []
            u_rdm = []
            
        d.append(data)
        u.append(u_rdm)
        
    _, tail = os.path.split(file_mask)
    test = tail.split('.')[0]

    D = {'RoiNames': V['RoiNames'], 'Data': d}
    df_d = pd.DataFrame(data=D)
    file_pickle = os.path.join(os.getcwd(), test + '_data.pkl')
    df_d.to_pickle(file_pickle)

    U = {'RoiNames': V['RoiNames'], 'Data': u}
    df_u = pd.DataFrame(data=U)
    file_pickle_u = os.path.join(os.getcwd(), test + '_rdm.pkl')
    df_u.to_pickle(file_pickle_u)
    
    files_out = [file_pickle, file_pickle_u]
    
    return files_out

def voi(Subject, file_mask, phase=1, model='H'):
    
    import os
    import pandas as pd
    import numpy as np
    from nilearn.image import load_img, index_img
    from nilearn.input_data import NiftiMasker
    
    from flow import Paths
    from flow.design import get_design
    from flow.rsa import get_score

    D = pd.DataFrame(columns=['subject', 'phase', 'model', 'islearned', 'voi', 'num_vox', 'score'])
    D = D.astype({'islearned': str})
    
    Analyses = [[[0], 'SeqId'], [[0,1], 'IsLearned'], [[1], 'SeqId']]
    
    File, Folder = Paths.SubjectData(Subject)
    
    V = pd.read_table(File['TsvAparc_2009'], index_col=0)

    # fmri_img = index_img(File['spmT'], Filter)
    fmri_img = load_img(File['spmT'])
    mask_img = load_img(File['Mask'][0])
    
    for voi in V.index:
        
        print(str(voi) + ' ' + V['RoiNames'][voi][:5] + ' ...', end='\r')

        th_img = index_img(file_mask, voi)
        num_vox = th_img.get_data().sum()
    
        if num_vox > 30:
            
            Masker = NiftiMasker(mask_img=th_img, standardize=True)     
            data = Masker.fit_transform(fmri_img)
            # print(data.shape)

            for a in Analyses:
                
                islearned = a[0]
                labelname = a[1]
                islearned_str = ''.join([str(l) for l in islearned])
                print(islearned_str)

                FilterDict = {'Phase': [phase], 'IsLearned': islearned}
                Filter, Label = get_design(FilterDict, LabelName=labelname)

                score, _ , _ = get_score(Filter, Label, data, islearned=islearned_str, model=model)

                D = D.append({'subject': Subject, 'phase': phase, 'model': model, 'islearned': islearned_str, 
                              'voi': voi, 'num_vox': int(num_vox), 'score': score}, ignore_index=True)

    file_csv = os.path.join(os.getcwd(), model + '.csv')
    D.to_csv(file_csv)

    return file_csv


def get_score(Filter, Label, data, islearned='0', model='H', num_perms = 10 ** 3, distance_measure = 'correlation'):
    import numpy as np
    from scipy.spatial.distance import cdist
    from scipy.stats import spearmanr
    from sklearn.model_selection import cross_val_score, KFold
    from sklearn.svm import SVC
    from nilearn import signal
    from stats.matrix import upper_tri_vector
    from flow.design import get_RDM

    hRDM = get_RDM(Model=model)
    sRDM = hRDM[Filter][:, Filter]
    
    if islearned == '01': # test only for between-class type similarity, rathern than the whole RDM
        n = sRDM.shape[0]
        J = np.zeros((n, n), dtype=bool)
        IsLearned = np.array(Label, dtype=bool)
        J[np.ix_(~IsLearned, IsLearned)] = True  # filter novel-learned similarity cells

    minvox = 10
    
    F = data
    z = np.full_like(np.arange(F.shape[0]), np.nan, dtype=np.double)  # variable for score
    p = np.full_like(np.arange(F.shape[0]), np.nan, dtype=np.double)  # variable for pvalue
    h = np.full((num_perms + 1, data.shape[0]), np.nan)  # scores from permutations: columns are vois

    score = np.nan
    pvalue = np.nan
    P = np.full((num_perms+1, 1), np.nan)
    
    if len(F):

        FilterZero = F[0][:] != 0  # filter out zero-columns
        if np.sum(FilterZero) > minvox:

            F = signal.clean(F)
            D = F[Filter][:, FilterZero]

            # switch test type
            if islearned == '1': # two learned seqs
                CV = KFold(n_splits = len(Label))
                svc = SVC(kernel = 'linear', C = 1.0)
                cv_score = cross_val_score(svc, D, Label, cv = CV)
                score = cv_score.mean()

            elif islearned == '0':
                dRDM = cdist(D, D, distance_measure)
                u_s = upper_tri_vector(sRDM)
                u_d = upper_tri_vector(dRDM)
                r = spearmanr(u_s, u_d)
                score = r.correlation
                pvalue = r.pvalue

            elif islearned == '01':
                dRDM = cdist(D, D, distance_measure)
                u_s = sRDM[J]
                u_d = dRDM[J]
                r = spearmanr(u_s, u_d)
                score = r.correlation
                pvalue = r.pvalue

            else:
                raise ValueError('No test match')

            # permutations
            for j in range(num_perms):
                if islearned == '1':
                    p_cv_score = cross_val_score(svc, D, np.random.permutation(Label), cv = CV)
                    P[j] = p_cv_score.mean()
                else:  # rsa type
                    pr = spearmanr(np.random.permutation(u_s), u_d)
                    P[j] = pr.correlation
            P[-1:] = score
            # # proportion of values smaller than test value
            # p = np.sum(P < score) / num_perms

    h = P.reshape((num_perms + 1))

    return score, pvalue, h
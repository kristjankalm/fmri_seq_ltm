import os, sys
os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path

def voi_run_dist(Subject, phase, file_pickle, metric='cosine'):

    from scipy import io
    from flow.design import get_design
    from scipy.spatial.distance import cdist, pdist, squareform
    from nilearn import signal
    from flow import Paths

    File, _ = Paths.SubjectData(Subject)
    data = pd.read_pickle(File['VoisData'])
    minvox = 10

    Analyses = (
        ['dRR', [1], 'SeqId'],      # between two repeating
        ['dUR', [0,1], 'SeqId'],    # between Repeating and Unique
        ['dU',  [0], 'TrialNum'],   # between consecutive Unique
        ['dR',  [1], 'TrialNum'],   # between consecutive Repeating
    )

    Z = pd.DataFrame(columns={'Label', 'd', 'Subject', 'Voi', 'labelname', 'analysis', 'phase'})

    for a in Analyses:

        model = a[0]
        islearned = a[1]
        labelname = a[2]

        islearned_str = ''.join([str(l) for l in islearned])
        name = model + '_' + islearned_str

        # define filter
        P = {'Phase': [phase], 'IsLearned': islearned}  # 'IsLearned': [0] == novel sequences only
        Filter, Label = get_design(Subject, P, labelname, verbose=False)

        # design matrix
        od = 1 # how many off-diagonal 
        X = np.diag(np.ones(len(Label)-od), od)

        if model == 'dRR': # between two repeating seq
            xF = np.array(Label.values == 2)
            X[xF] = 0
        elif model == 'dUR': # between two repeating seq
            xF = np.array(Label.values != 2)
            X[xF] = 0
        
        # iterate over vois
        for i in range(0, data.shape[0]):

            F = data['Data'][i]
            if len(F):

                FilterZero = F[0][:] != 0  # filter out zero-columns

                if np.sum(FilterZero) > minvox:

                    F = signal.clean(F)
                    D = F[Filter][:, FilterZero]

                    B = squareform(pdist(D, metric=metric))
                    d = B[np.where(X)]

                    # add to dataframe
                    Y = pd.DataFrame(columns=Z.columns)
                    Y.Label = np.arange(len(d))
                    Y.d = d
                    Y.Subject = [Subject for j in d]
                    Y.Voi = [i for j in d]
                    Y.labelname = [labelname for j in d]
                    Y.analysis = [name for j in d]
                    Y.phase = [phase for j in d]

                    Z = Z.append(Y, ignore_index=True)

    # write rho values
    # csv_file = os.path.join(os.getcwd(), 'dist_' + str(phase) + '.tsv')
    # Z.to_csv(csv_file, sep='\t')

    # return csv_file
    return Z

def fmri_noise(Subject):
    
    import os
    import numpy as np
    from flow.Paths import SubjectData
    from nilearn.image import load_img, new_img_like
    from brainiak.utils import fmrisim
    
    File, _ = SubjectData(Subject)
    
    file_epi = File['Bold'][0]
    i = load_img(file_epi)
    dimsize = i.header.get_zooms()
    volume = i.get_data()
    mask, template = fmrisim.mask_brain(volume=volume, mask_self=True)
    
    noise_dict = {'voxel_size': [dimsize[0], dimsize[1], dimsize[2]], 'matched': 1}
    
    print('Estimating noise parameters ...')
    
    noise_dict = fmrisim.calc_noise(
        volume = volume,
        mask = mask,
        template = template,
        noise_dict = noise_dict,
        )
    
    print('Noise parameters of the data were estimated as follows:')
    print('SNR: ' + str(noise_dict['snr']))
    print('SFNR: ' + str(noise_dict['sfnr']))
    print('FWHM: ' + str(noise_dict['fwhm']))
    
    dim = volume.shape
    tr = dimsize[3]

    print('Estimating noise for the volume ...')
    noise = fmrisim.generate_noise(
        dimensions = dim[0:3],
        tr_duration = int(tr),
        stimfunction_tr = [0] * dim[3],
        mask = mask,
        template = template,
        noise_dict = noise_dict
    )
    
    noise_image = new_img_like(file_epi, noise.astype(np.int16))
    file_noise_img = os.path.join(os.getcwd(), 'noise.nii.gz')
    print('Saving: ' + file_noise_img)
    noise_image.to_filename(file_noise_img)
    
    file_noise_dict = os.path.join(os.getcwd(), 'noise_dict.npy')
    np.save(file_noise_dict, noise_dict)
    
    return file_noise_dict, file_noise_img
    
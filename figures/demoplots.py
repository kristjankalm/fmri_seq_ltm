import sys
import os
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from filterpy import stats as fs
from scipy.stats import entropy

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path

sns.set_style("white");
sns.despine(offset=4, trim=True);
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5});

def plot_bayes(mu, s, fill_posterior=False, display_bits=False):
    
    x = np.linspace(-12, 12, 120)
    gy = fs.gaussian(x, mu[0], s[0], normed=True)
    gz = fs.gaussian(x, mu[1], s[1], normed=True)
    (pmu, psigma) = fs.mul(mu[0], s[0], mu[1], s[1])
    gp = fs.gaussian(x, pmu, psigma, normed=True)

    fig = plt.figure(figsize=(8,4));
    ax = sns.lineplot(x=x, y=gy)
    sns.lineplot(x=x, y=gz, ax=ax)
    sns.lineplot(x=x, y=gp, ax=ax)
    
    # get line cols
    colours = [fig.gca().lines[i].get_color() for i in range(3)]
    
    # get data
    X = []
    Y = []
    for i in range(3):
        l1 = ax.lines[i]
        X.append(l1.get_xydata()[:,0])
        Y.append(l1.get_xydata()[:,1])
    
    if fill_posterior:
        ax.fill_between(X[2], Y[2], color=colours[2], alpha=0.3)
        
        
    sns.despine(left=True, bottom=True)
    # ax.set_ylabel('Response probability')
    ax.set_yticklabels('')
    ax.set_xticks([mu[0]])
    ax.set_xticklabels(['y'], color = colours[0])
    ax.set_ylim([-0.01,0.15])
    ax.set_xlim([-10,10])
    
    # labels
    lsize = 16
    yoff = 0.01
    ax.text(mu[0] - 1.7, np.max(Y[0]) + yoff, 'p(y|z)', color=colours[0], size=lsize)
    ax.text(mu[1], np.max(Y[1]) + yoff, 'p(z)', color=colours[1], size=lsize)
    if display_bits:
        pbits = round(entropy(Y[2]), 4)
        ax.text(pmu - 0.1,   np.max(Y[2]) + yoff, 'p(z|y) : ' + str(pbits) + ' bits',  color=colours[2],  size=lsize)
    else:
        ax.text(pmu - 0.1,   np.max(Y[2]) + yoff, 'p(z|y)',  color=colours[2],  size=lsize)
    
    ax.scatter(x=mu[0], y=0)
    
    return [Y, X]

def scatter_post(y, x):
    fig = plt.figure(figsize=(8,4));
    ax = sns.scatterplot(y = y, x = x, color='green');
    ax.vlines(x, ymin=0, ymax=y, color='green', lw=0.5)
    sns.despine(left=True, bottom=True);
    ax.set_yticklabels('');
    ax.set_xticklabels('');
    ax.set_ylabel('Response magnitude', size=14);
    ax.set_xlabel('Response units with equally distributed means', size=14);
    ax.set_ylim([-0.01,0.15]);
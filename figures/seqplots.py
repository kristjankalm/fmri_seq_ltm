import sys
import os
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from math import sin, cos, radians

os.chdir(os.path.dirname(os.path.abspath(__file__))) # go to the folder of this script
sys.path.append(os.path.join(os.path.dirname(os.getcwd()))) # add parent folder to path
from seq import rep
from flow import design

def rotate_point(point, angle, center_point=(0, 0)):
    """Rotates a point around center_point(origin by default)
    Angle is in degrees.
    Rotation is counter-clockwise
    """
    angle_rad = radians(angle % 360)
    # Shift the point so that center_point becomes the origin
    new_point = (point[0] - center_point[0], point[1] - center_point[1])
    new_point = (new_point[0] * cos(angle_rad) - new_point[1] * sin(angle_rad),
                 new_point[0] * sin(angle_rad) + new_point[1] * cos(angle_rad))
    # Reverse the shifting we have done
    new_point = (new_point[0] + center_point[0], new_point[1] + center_point[1])
    return new_point


def make_gabor(ax, x, y, h, w, ang):
    n = 4
    xx = np.linspace(x-w/2, x+w/2, n)
    hh = [h-h/2, h*0.9, h*0.9, h-h/2]
    c = [rotate_point((xp, y), ang, (x, y)) for xp in xx]

    E = []
    for i, x in enumerate(xx):
        el = Ellipse(xy=c[i], width=w/n, height=hh[i], angle=ang)
        E.append(el)

    for i, e in enumerate(E):
        e.set_alpha(0.9)
        e.set_facecolor([0, 0, 0])
        ax.add_artist(e)


def plot_rep(M, model='ip', margin=0, **kwargs):

    if 'steps' in kwargs:
        steps = kwargs.get('steps')
    else:
        steps = 10

    if model == 'ip':
        xt = list(range(1, 5))
    else:
        xt = False

    plt.rcParams.update({'font.size': 20})
    f, ax = plt.subplots(figsize=(3, 3))
    cmap = sns.cubehelix_palette(steps, start=0, rot=0, dark=0, light=.95)

    sns.heatmap(M, cmap=cmap, vmax=1, center=0.5, linewidths=4, ax=ax, xticklabels=xt, yticklabels=False, cbar=False,)

    xl, yl, xh, yh = np.array(ax.get_position()).ravel()
    w = xh - xl
    h = yh - yl
    xp = xl + w*0.1  # if replace '0' label, can also be calculated systematically using xlim()
    size = 0.13
    ticks = np.linspace(0.14, 0.73, 4)

    angles = [0, 45, 90, 135]  # angles for gabors
    gabor_height = 0.9
    gabor_width = 0.6

    for i, t in enumerate(ticks):

        fax = f.add_axes([margin, t, size, size])
        fax.axison = False
        make_gabor(fax, 0.5, 0.5, gabor_height, gabor_width, angles[i])

        if model == 'ii':
            fx = f.add_axes([t, margin, size, size])
            fx.axison = False
            make_gabor(fx, 0.5, 0.5, gabor_height, gabor_width, angles[i])

    return f


def plot_abcd(M, model='ip', **kwargs):
    
    abcd = ['A', 'B', 'C', 'D']
    # abcd = ['D', 'C', 'B', 'A']
    
    if 'steps' in kwargs:
        steps = kwargs.get('steps')
    else:
        steps = 10
 
    if model == 'ip':
        xt = list(range(1, 5))
    else:
        xt = abcd

    plt.rcParams.update({'font.size': 20})
    f, ax = plt.subplots(figsize=(3, 3))
    cmap = sns.cubehelix_palette(steps, start=0, rot=0, dark=0, light=.95)
    
    sns.heatmap(M, cmap=cmap, vmax=1, center=0.5, linewidths=4, xticklabels=xt, yticklabels=abcd, cbar=False,)

    ax.tick_params(labelsize=20)
    ax.tick_params(axis='y', labelrotation=0)

    return f



def plot_seq_gabor(S):

    f, ax = plt.subplots(figsize=(4, 1))
    ax.axison = False

    xl, yl, xh, yh = np.array(ax.get_position()).ravel()
    w = xh - xl
    h = yh - yl
    xp = xl + w*0.1  # if replace '0' label, can also be calculated systematically using xlim()
    size = 0.7
    margin = 0.13
    ticks = np.linspace(0.13, 0.7, 4)

    Angles = [0, 45, 90, 135]  # angles for gabors
    angles = [Angles[s] for s in S-1]

    gabor_height = 0.9
    gabor_width = 0.6

    for i, t in enumerate(ticks):
        fx = f.add_axes([t, margin, size/4, size])
        fx.axison = False
        make_gabor(fx, 0.5, 0.5, gabor_height, gabor_width, angles[i])

    return f


def plot_rdm(D, model='rsa', cbar=True, vmax=2/3, center=1/2, linewidths=2, **kwargs):
    import warnings

    warnings.filterwarnings('ignore')

    if 'cmap' in kwargs:
        cmap = kwargs.get('cmap')
    else:
        cmap = sns.diverging_palette(220, 10, as_cmap=True)

    sns.set(style="white")

    # Generate a mask for the upper triangle
    mask = np.zeros_like(D, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    #
    labels = list(range(1, D.shape[1]+1))

    # Draw the heatmap with the mask and correct aspect ratio
    if len(labels) < 20:
        xticklabels = labels
        yticklabels = labels
    else:
        xticklabels = False
        yticklabels = False

    f = sns.heatmap(D, mask=mask, cmap=cmap, vmax=vmax, center=center,
                    xticklabels=xticklabels, yticklabels=yticklabels, square=True,
                    linewidths=linewidths, cbar=cbar, cbar_kws={"shrink": .5}, **kwargs)

    # # this for paper RDMS
    # f = sns.heatmap(D, mask=mask, cmap=cmap, vmax=1, center=1/2,
    #     xticklabels=labels, yticklabels=labels,
    #     square=True, linewidths=2, cbar=False, cbar_kws={"shrink": .5})

    if 'box' in kwargs:
        box = kwargs.get('box')
        lw = 2
        lc = 'black'
        y1 = box
        y2 = D.shape[0] - 0.1
        x1 = 0.1
        x2 = y1

        plt.plot([x1, x2], [y1, y1], linewidth=lw, color=lc)
        plt.plot([x2, x2], [y1, y2], linewidth=lw, color=lc)
        plt.plot([x1, x1], [y1, y2], linewidth=lw, color=lc)
        plt.plot([x1, x2], [y2, y2], linewidth=lw, color=lc)

    return f


def plot_seqs(Seq, model='ip', sigma=0):

    matrix_seq = rep.seq_unit_matrix(Seq, model=model, sigma=sigma)

    abcd = ['A', 'B', 'C', 'D']
    if model == 'ip':
        labels_abcd = [[a + str(n + 1) for n in np.arange(4)] for a in abcd]
    if model == 'ii':
        labels_abcd = [[a + n for n in abcd] for a in abcd]

    x_abcd = np.array(labels_abcd).reshape(1, 4 ** 2)[0]
    y_abcd = [''.join([abcd[i - 1] for i in s]) for s in Seq]

    cmap = sns.cubehelix_palette(10, start=0, rot=0, dark=0, light=.95)

    ax = sns.heatmap(matrix_seq, cmap=cmap, vmax=1, center=0.5, cbar=False, linewidths=6,
                     xticklabels=x_abcd, yticklabels=y_abcd)
    ax.tick_params(labeltop=True, labelbottom=False, labelsize=14, bottom=False)

    return ax


def plot_rdm_ordered(Label, Filter, model='H'):
    # reordering info
    idx = range(Label.shape[0])
    L = pd.DataFrame(data={'Label':Label, 'Idx': range(Label.shape[0])}, index=Label.index)
    L = L.sort_values('Label')
    reorder_idx = L['Idx']

    # stimulus RDM
    hRDM = design.get_RDM(Model=model) # hamming
    sRDM = hRDM[Filter][:, Filter]
    sRDM_reo = sRDM[reorder_idx][:,reorder_idx]
    ax = sns.heatmap(sRDM_reo);
    
    return ax
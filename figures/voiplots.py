import os, sys
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
    
def errplot(x, y, yerr, **kwargs):
    cmap = sns.cubehelix_palette(3, start=.8, rot=-.75, dark=0.4, reverse=True)
    ax = plt.gca()
    data = kwargs.pop('data')
    c = data.index[0]
    data.plot(x=x, y=y, yerr=yerr, kind='bar', width=1, ax=ax, color=cmap[c])
    ax.tick_params(axis='y', labelsize=13)
    sns.despine(offset=10, trim=False, bottom=True, left=True, right=True, ax=ax);

def noise_line(B, **kwargs):
    ax = plt.gca()
    lineargs = {'linewidth':1.5, 'linestyle':'dotted', 'color':'black'}
    ax.plot([-0.5, 1.2], [B.mean(), B.mean()], **lineargs)
    
def label(phase, **kwargs):
    ax = plt.gca()
    ax.text(-0.3, -0.05, phase.unique()[0], fontsize=16)

def asterisk(sig, phase, y, e, **kwargs):
    if sig.values[0][0] == phase.values[0]:
        ax = plt.gca()
        ax.text(-0.2, y + e + 0.01, '*', fontsize=16, color='red')
    
def sem(y):
    return np.nanstd(y) / np.sqrt(y.shape[0] - 1) 

    
def noise_phase(bg, **kwargs):
    
    g = sns.FacetGrid(bg, col="Phase", height=2.7, aspect=.25)
    g.map_dataframe(errplot, 'Phase', 'R', 'sem')
    g.map(noise_line, 'LB')
    g.map(noise_line, 'UB')
    g.map(label, 'Phase')
    g.map(asterisk, 'sig', 'Phase', 'R', 'sem')
    g.set(ylim=(0, 0.4), yticks=[0, 0.1, 0.2, 0.3, 0.4], ylabel='', xlabel='', xticklabels='')
    g.set_titles(col_template="")
    g.fig.subplots_adjust(wspace=-0.2)
    g.fig.suptitle(bg['Name'].values[0], y=1.05, x=0.6, fontsize=14);
    return g
    

def svm(D, **kwargs):
    # D is dataframe
    cmap_1 = sns.diverging_palette(220, 10, sep=80, n=2)
    cmap_2 = sns.diverging_palette(220, 10, sep=80, n=2, l=78)

    ax = sns.stripplot(data=D, jitter=0.01, palette=cmap_1, **kwargs)
    ax = sns.pointplot(data=D, join=False, palette=cmap_2, ax=ax, errwidth=20, **kwargs)
    # mod plot
    ax.set_xlabel('')
    ax.tick_params(axis='x', labelsize=24, bottom=False)
    ax.tick_params(axis='y', labelsize=16)
    ax.yaxis.label.set_size(18)
    plt.subplots_adjust(left=0.35, right=0.99)
    sns.despine(offset=20, trim=False, bottom=True, ax=ax);

    return ax


def rsa_and_svm(pal_1, pal_2, chancelevel=0, **kwargs):
    ax = sns.swarmplot(palette=pal_2, alpha=0.25, **kwargs)
    ax = sns.pointplot(join=False, errwidth=3, palette=pal_1, scale=1.5, capsize=0.1, **kwargs)
    ax.plot(ax.get_xlim(), [chancelevel, chancelevel], linewidth=2, linestyle='dotted', color='black')
    ax.set_ylabel('')
    ax.set_xlabel('')
    ax.yaxis.label.set_size(18)
    ax.title.set_fontsize(18)
    ax.tick_params(axis='y', labelsize=14)
    ax.tick_params(axis='x', labelsize=18, bottom=False)
    sns.despine(offset=20, trim=False, bottom=True, ax=ax);
    return ax


def model(i, chancelevel=0, **kwargs):
    
    data = kwargs.get('data')
    
    channels = [0.92, 2, -0.1]
    d = 0.5
    l = 0.3
    w = 0.15
    rot = 0
    n_col = 4
    
    ch = channels[i]
    
    pal_1 = sns.color_palette('ch:start=' + str(ch) + ',rot=' + str(rot) + \
                              ',dark=' + str(d) + ', hue=3, light=' + str(l), n_colors=n_col)
    pal_2 = sns.color_palette('ch:start=' + str(ch) + ',rot=' + str(rot) + \
                               ',dark=' + str(d) + ',hue=2, light=' + str(l), n_colors=n_col)
    
    
    ax = sns.swarmplot(palette=pal_2, alpha=0.2, **kwargs)
    ax = sns.pointplot(ci=95, join=False, errwidth=3, palette=pal_1, scale=1.5, capsize=0.1, **kwargs)
    ax.plot(ax.get_xlim(), [chancelevel, chancelevel], linewidth=2, linestyle='dotted', color='black')
    
    la = {'color': pal_1[0], 'linewidth': 1.5, 'linestyle': 'dotted'}
    ax.plot(ax.get_xlim(), [data.UB.values[0], data.UB.values[0]], **la)
    ax.plot(ax.get_xlim(), [data.LB.values[0], data.LB.values[0]], **la)

    
    ax.set_ylabel('')
    ax.set_xlabel('')
    ax.yaxis.label.set_size(18)
    ax.title.set_fontsize(18)
    ax.tick_params(axis='y', labelsize=14)
    ax.tick_params(axis='x', labelsize=18, bottom=False)
    sns.despine(offset=20, trim=False, bottom=True, ax=ax);
    
    #ax.set_xticklabels(['R','AL']);
    ax.set_ylim(-0.15,0.35)
    
    return ax
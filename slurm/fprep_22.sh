#!/bin/bash
#SBATCH --job-name=fprep
#SBATCH --qos=normal
#SBATCH --time=72:20:00
#SBATCH --nodelist="node-i04"
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=24
#SBATCH --array=0
#SBATCH --mem-per-cpu=8GB
#SBATCH --output=log/o_22_%A_%a.txt
#SBATCH --error=log/e_22_%A_%a.txt
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kristjan.kalm@gmail.com

export dir_bids=/home/kk02/matlab/data/f/bids
export dir_out=/home/kk02/matlab/data/f/fprep
export dir_tmp=/home/kk02/matlab/data/f/fprep/tmp
export dir_sing=/imaging/local/software/singularity_images
export simg_fprep=fmriprep/fmriprep-1.2.0.simg
export fs_lic=/imaging/local/software/freesurfer/6.0.0/x86_64/license.txt

SUBS=(22)
singularity run -B /imaging $dir_sing/$simg_fprep --participant_label ${SUBS[$SLURM_ARRAY_TASK_ID]} --fs-license-file $fs_lic --notrack --stop-on-first-crash --write-graph --verbose --no-submm-recon --omp-nthreads 1 $dir_bids $dir_out participant -w $dir_tmp

echo ""
echo "${SUBS[$SLURM_ARRAY_TASK_ID]}  ༼ つ ◕_◕ ༽つ finished "
echo ""

#!/bin/bash
#SBATCH --job-name=nipype
#SBATCH --qos=normal
#SBATCH --time=72:20:00
#SBATCH --array=0-19
#SBATCH --mem-per-cpu=64GB
# #SBATCH --cpus-per-task=3
#SBATCH --output=log/o_%A_%a.txt
#SBATCH --error=log/e_%A_%a.txt
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kristjan.kalm@gmail.com

export dir_nipype=/home/kk02/Code/python/ltm/nipype_flows
export py3=/imaging/local/software/anaconda/2.4.1/2/envs/p36/bin/python

SUBS=(01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 19 20 21)

$py3 $dir_nipype/rsa.py ${SUBS[$SLURM_ARRAY_TASK_ID]}

echo ""
echo "${SUBS[$SLURM_ARRAY_TASK_ID]}  ༼ つ ◕_◕ ༽つ finished "
echo ""

#!/bin/bash

export dir_bids=/home/kk02/matlab/data/f/bids
export dir_out=/home/kk02/matlab/data/f/fprep
export dir_tmp=/home/kk02/matlab/data/f/fprep/tmp
export dir_sing=/imaging/local/software/singularity_images
export simg_fprep=fmriprep/fmriprep-1.2.0.simg
export fs_lic=/imaging/local/software/freesurfer/6.0.0/x86_64/license.txt

singularity run -B /imaging $dir_sing/$simg_fprep --participant_label 04 --fs-license-file $fs_lic --notrack --stop-on-first-crash --write-graph --verbose --no-submm-recon $dir_bids $dir_out participant -w $dir_tmp

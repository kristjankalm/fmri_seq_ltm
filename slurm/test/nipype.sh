#!/bin/bash
#SBATCH --job-name=nipype
#SBATCH --qos=normal
#SBATCH --time=24:20:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4#SBATCH --mem-per-cpu=8GB
#SBATCH --output=o_nipype_%A_%a.txt
#SBATCH --error=e_nipype_%A_%a.txt
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kristjan.kalm@gmail.com

bash pyenv.sh
